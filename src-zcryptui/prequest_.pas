unit prequest;
// Password input form


{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  Buttons,
  DCPsha256,
  DCPrijndael;

type

  { TfrmPrequest }

  TfrmPrequest = class(TForm)
    btnOK: TButton;
    btnCancel: TButton;
    edtp1: TEdit;
    lblContainerPW: TLabel;
    lblContainerPW1: TLabel;
    sbtnSeePwd: TSpeedButton;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure edtp1KeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
    procedure sbtnSeePwdMouseEnter(Sender: TObject);
    procedure sbtnSeePwdMouseLeave(Sender: TObject);
  private

  public

  end;

var
  frmPrequest: TfrmPrequest;
//pho, pwo: string;

implementation

uses main;

{$R *.lfm}

{ TfrmPrequest }

procedure TfrmPrequest.btnOKClick(Sender: TObject);
begin
  frmPrequest.Close;
end;

procedure TfrmPrequest.edtp1KeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then
  begin
    Key := #0;
    btnOK.Click; // oder etwas anderes
  end;
end;

procedure TfrmPrequest.btnCancelClick(Sender: TObject);
begin
  edtp1.Clear;
  frmPrequest.Close;
end;

procedure TfrmPrequest.FormShow(Sender: TObject);
begin
  edtp1.SetFocus;
end;

procedure TfrmPrequest.sbtnSeePwdMouseEnter(Sender: TObject);
begin
   edtp1.EchoMode := emNormal;
end;

procedure TfrmPrequest.sbtnSeePwdMouseLeave(Sender: TObject);
begin
  edtp1.EchoMode := emPassword;
end;

end.
