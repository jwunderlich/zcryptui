unit synclog;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Grids, StdCtrls,
  XMLPropStorage, config, process, TypInfo, Crt;

type

  { TfrmSyncLog }

  TfrmSyncLog = class(TForm)
    btnClose: TButton;
    grdSyncLog: TStringGrid;
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);

    //set Font in App at Runtime
    procedure SetFontProperties(Control: TControl; FntName: TFontName;
      Size: integer; Styles: TFontStyles);

  private

  public

  end;

var
  frmSyncLog: TfrmSyncLog;

implementation

{$R *.lfm}


//Set Font at Runtime
//https://stackoverflow.com/questions/10588660/font-consistency-throughout-project
//Using: SetFontProperties(Self, 'Ubuntu Condensed', 10, []);
//Checking for Condensed Fonts on Linux via Terminal Command:
//fc-list |grep 'Condensed'
procedure TfrmSyncLog.SetFontProperties(Control: TControl; FntName: TFontName;
  Size: integer; Styles: TFontStyles);
// Set font properties
var
  Index: integer;
  FntFont: TFont;
  AnObject: TObject;
  ChildControl: TControl;
begin
  // Set FntFont properties
  AnObject := GetObjectProp(Control, 'Font', nil);
  if AnObject is TFont then
  begin
    // Set properties
    FntFont := TFont(AnObject);
    FntFont.Name := FntName;
    FntFont.Size := Size;
    FntFont.Style := Styles;
  end;

  // Set child FntFont properties
  if Control is TWinControl then
  begin
    // Set
    for Index := 0 to TWinControl(Control).ControlCount - 1 do
    begin
      // Child control
      ChildControl := TWinControl(Control).Controls[Index];

      // Set FntFont properties
      SetFontProperties(ChildControl, FntName, Size, Styles);
    end;
  end;
end;  //SetFontProperties

{ TfrmSyncLog }
procedure TfrmSyncLog.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSyncLog.FormCreate(Sender: TObject);
var
  logf: string; //log info name
  fontexists: Boolean; //check if ubuntu condensed font exists
  S: TResourceStream; //Resource Content Handling
  F: TFileStream;  //Resource Content Handling
  Process: TProcess; //for font cache refresh command (cmd)
  aProcess: TProcess; //restarting self

begin
  fontexists := FileExists(GetUserDir + '/.fonts/ucondensed.ttf');

  if not fontexists then
  begin

    //     LOAD CUSTOM FONT
    //Create local Font Dir for Ubuntu Condensed Font, if not exists
    if not DirectoryExists(GetUserDir + '/.fonts') then
      if not CreateDir(GetUserDir + '/.fonts') then
        ShowMessage('Failed to create local font directory!');
      //else
       // memEvents.Lines.Add(ts + ' Local font directory created');

    //Using resources:
    //https://wiki.freepascal.org/Lazarus_Resources#Linux
    //https://wiki.freepascal.org/IDE_Window:_Project_Options#Resources
    //https://wiki.freepascal.org/fcl-res
    //extract the stored resource (font) into the file ucondensed.ttf_:
    // create a resource stream which points to our resource
    S := TResourceStream.Create(HInstance, 'UCONDENSED', RT_RCDATA);
    // Please ensure you write the enclosing apostrophes around DATA,
    // otherwise no data will be extracted.
    try
      // create a file ucondensed.ttf_ in the application directory
      //F := TFileStream.Create(ExtractFilePath(ParamStr(0)) + 'ucondensed.ttf_', fmCreate);
      F := TFileStream.Create(GetUserDir + '/.fonts/ucondensed.ttf', fmCreate);
      try
        F.CopyFrom(S, S.Size); // copy data from the resource stream to file stream
      finally
        F.Free; // destroy the file stream
      end;
    finally
      S.Free; // destroy the resource stream
    end;

    //Refresh Font Cache (for Ubuntu Condensed)
    Process := TProcess.Create(nil);
    //$ fc-cache -fv
    try
      Process.CommandLine := 'fc-cache -fr';
      Process.Options := Process.Options + [poWaitOnExit];
      Process.Execute;
      Process.Free;
    except
      Process.Free;
      //ShowMessage('File Date checking Process: Exception occured');
      ShowMessage('Error while refreshing Font Cache at Start');
    end; //try

    //Setting Font (Ubuntu Condensed from Resource File) Recursively for all components
    // Not Working on Linux, only in Windows: Application.DefaultFont := 'Ubuntu Condensed';
    try
      SetFontProperties(Self, 'Ubuntu Condensed', 11, []);
      //SetFontProperties(Self, 'Nimbus Sans Narrow Regular', 10, []);
    except
      ShowMessage('Font [Ubuntu Condensed] does not exists');
    end;

    //after copying new font, restart Application to apply changes
    Delay(2000);
    aProcess := TProcess.Create(nil);
    aProcess.CommandLine := '"' + Application.ExeName + '"';
    aProcess.Execute;
    aProcess.Free;
    Application.Terminate;
  end
  else
    SetFontProperties(Self, 'Ubuntu Condensed', 11, []);

  //     END LOAD CUSTOM FONT

  //load Data on creating form, if logfile exists
  logf := ExtractFilePath(ParamStr(0)) + '.' + ExtractFileName(ParamStr(0)) + 'log';
  if FileExists(logf) then
    frmSyncLog.grdSyncLog.LoadFromFile(logf);
end; //Create

procedure TfrmSyncLog.FormShow(Sender: TObject);
var
  i: integer;
begin
  for i := 1 to 9 do
  begin
    grdSyncLog.Cells[0, i] := config.Containers[i].Name;
  end;
end;

end.
