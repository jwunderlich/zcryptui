unit zcwait;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

type

  { TfrmZcWait }

  TfrmZcWait = class(TForm)
    lblMsgTitle: TLabel;
    StaticText1: TStaticText;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  frmZcWait: TfrmZcWait;

implementation

uses main;

{$R *.lfm}

{ TfrmZcWait }

procedure SetCursorWait;
var
  i: integer;
begin
  for i := 0 to frmMain.ControlCount - 1 do
    frmMain.Controls[i].Cursor := crHourGlass;
end;

procedure SetCursorNormal;
var
  i: integer;
begin
  for i := 0 to frmMain.ControlCount - 1 do
    frmMain.Controls[i].Cursor := crDefault;
end;



procedure TfrmZcWait.FormShow(Sender: TObject);
begin
  SetCursorWait;

end;


procedure TfrmZcWait.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  SetCursorNormal;
end;

end.
