unit vinfo;
// Show Application Version within your program
// Linux: There is no implicit provision in the ELF file format for version numbers,
//        copyright and so on, but Lazarus stores this in resource sections.
//        Reading the information back at runtime relies on FPC units,
//        below is some useful information.
//        https://wiki.freepascal.org/Show_Application_Title,_Version,_and_Company#Linux
// https://forum.lazarus.freepascal.org/index.php/topic,12435.0.html
// Credits:
// Paul Ishenin - for the vinfo.pas unit and the code.

// Sample Code:
{*
procedure TfMain.FormCreate(Sender: TObject);
// initialize a bunch of stuff for this app when the form is first opened

     // [0] = Major version, [1] = Minor ver, [2] = Revision, [3] = Build Number
     // The above values can be found in the menu: Project > Project Options > Version Info

Var
  // [0] = Major version, [1] = Minor ver, [2] = Revision, [3] = Build Number
  // The above values can be found in the menu: Project > Project Options > Version Info
  verMajor, verMinor, verRev, verBuild : String;
  Info: TVersionInfo;

begin
  Info := TVersionInfo.Create;
  // [0] = Major version, [1] = Minor ver, [2] = Revision, [3] = Build Number
  // The above values can be found in the menu: Project > Project Options > Version Info
  Info.Load(HINSTANCE);
  // grab the entire Version Number
  // verMajor, verMinor, verRev, verBuild : String;
  verMajor := IntToStr(Info.FixedInfo.FileVersion[0]);
  verMinor := IntToStr(Info.FixedInfo.FileVersion[1]);
  verRev := IntToStr(Info.FixedInfo.FileVersion[2]);
  verBuild := IntToStr(Info.FixedInfo.FileVersion[3]);
  Info.Free;
  // Update the title string - include the version & ver #
  frmMain.Caption := 'zcryptui' + ' v'+verMajor+'.'+verMinor+'.'+verRev+'.'+verBuild;
*}
// END of Sample Code

{$mode objfpc}

interface

uses
  Classes, SysUtils, resource, versiontypes, versionresource;

type
  { TVersionInfo }

  TVersionInfo = class
  private
    FVersResource: TVersionResource;
    function GetFixedInfo: TVersionFixedInfo;
    function GetStringFileInfo: TVersionStringFileInfo;
    function GetVarFileInfo: TVersionVarFileInfo;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Load(Instance: THandle);
    property FixedInfo: TVersionFixedInfo read GetFixedInfo;
    property StringFileInfo: TVersionStringFileInfo read GetStringFileInfo;
    property VarFileInfo: TVersionVarFileInfo read GetVarFileInfo;
  end;

implementation

{ TVersionInfo }

function TVersionInfo.GetFixedInfo: TVersionFixedInfo;
begin
  Result := FVersResource.FixedInfo;
end;

function TVersionInfo.GetStringFileInfo: TVersionStringFileInfo;
begin
  Result := FVersResource.StringFileInfo;
end;

function TVersionInfo.GetVarFileInfo: TVersionVarFileInfo;
begin
  Result := FVersResource.VarFileInfo;
end;

constructor TVersionInfo.Create;
begin
  inherited Create;
  FVersResource := TVersionResource.Create;
end;

destructor TVersionInfo.Destroy;
begin
  FVersResource.Free;
  inherited Destroy;
end;

procedure TVersionInfo.Load(Instance: THandle);
var
  Stream: TResourceStream;
begin
  Stream := TResourceStream.CreateFromID(Instance, 1, PChar(RT_VERSION));
  try
    FVersResource.SetCustomRawDataStream(Stream);
    // access some property to load from the stream
    FVersResource.FixedInfo;
    // clear the stream
    FVersResource.SetCustomRawDataStream(nil);
  finally
    Stream.Free;
  end;
end;

end.


