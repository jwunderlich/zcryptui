unit config;
// configuration form

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, EditBtn,
  XMLPropStorage, StrUtils, Math, Buttons, ComCtrls, LCLType, ExtCtrls,
  ButtonPanel, LazFileUtils,
  uappisrunning, Process;
// FileUtil;

type

  { TfrmConfig }

  TfrmConfig = class(TForm)
    btnClrEdt21: TSpeedButton;
    btnClrEdt11: TSpeedButton;
    btnClrEdt12: TSpeedButton;
    btnClrEdt13: TSpeedButton;
    btnClrEdt14: TSpeedButton;
    btnClrEdt15: TSpeedButton;
    btnClrEdt16: TSpeedButton;
    btnClrEdt17: TSpeedButton;
    btnClrEdt18: TSpeedButton;
    btnClrEdt19: TSpeedButton;
    btnClrEdt2: TSpeedButton;
    btnClrEdt22: TSpeedButton;
    btnClrEdt23: TSpeedButton;
    btnClrEdt3: TSpeedButton;
    btnClrEdt4: TSpeedButton;
    btnClrEdt5: TSpeedButton;
    btnClrEdt6: TSpeedButton;
    btnClrEdt7: TSpeedButton;
    btnClrEdt8: TSpeedButton;
    btnClrEdt9: TSpeedButton;
    btnExtraSync: TSpeedButton;
    btnInfo: TSpeedButton;
    ButtonPanel1: TButtonPanel;
    chkAutoHide: TCheckBox;
    chkHideOnFocusLost: TCheckBox;
    chkMntSyncAuto: TCheckBox;
    chkUmountSynced: TCheckBox;
    chkExtraSyncOn: TCheckBox;
    chkFirstRun: TCheckBox;
    chkFlushAfterMount: TCheckBox;
    chkFlushOnMinimize: TCheckBox;
    chkStartMinimized: TCheckBox;
    chkStayOnTop: TCheckBox;
    cmbPwTypC1: TComboBox;
    cmbPwTypC2: TComboBox;
    cmbPwTypC3: TComboBox;
    cmbPwTypC4: TComboBox;
    cmbPwTypC5: TComboBox;
    cmbPwTypC6: TComboBox;
    cmbPwTypC7: TComboBox;
    cmbPwTypC8: TComboBox;
    cmbPwTypC9: TComboBox;
    edtExtDrivePath: TDirectoryEdit;
    edtPWtypName1: TEdit;
    edtPWtypName2: TEdit;
    edtCont1: TFileNameEdit;
    edtExtra1: TFileNameEdit;
    edtCont11: TFileNameEdit;
    edtCont12: TFileNameEdit;
    edtCont13: TFileNameEdit;
    edtCont14: TFileNameEdit;
    edtCont15: TFileNameEdit;
    edtCont16: TFileNameEdit;
    edtCont17: TFileNameEdit;
    edtCont18: TFileNameEdit;
    edtCont19: TFileNameEdit;
    edtCont2: TFileNameEdit;
    edtCont3: TFileNameEdit;
    edtCont4: TFileNameEdit;
    edtCont5: TFileNameEdit;
    edtCont6: TFileNameEdit;
    edtCont7: TFileNameEdit;
    edtCont8: TFileNameEdit;
    edtCont9: TFileNameEdit;
    edtSynProfile1: TFileNameEdit;
    edtSynProfile2: TFileNameEdit;
    edtSynProfile3: TFileNameEdit;
    edtSynProfile4: TFileNameEdit;
    edtSynProfile5: TFileNameEdit;
    edtSynProfile6: TFileNameEdit;
    edtSynProfile7: TFileNameEdit;
    edtSynProfile8: TFileNameEdit;
    edtSynProfile9: TFileNameEdit;
    edtExtra2: TFileNameEdit;
    edtExtra3: TFileNameEdit;
    grpAppearance: TGroupBox;
    grpExtraSync: TGroupBox;
    grpContainers: TGroupBox;
    grpContainers1: TGroupBox;
    grpFlushPw: TGroupBox;
    grpInterface: TGroupBox;
    grpPWtyp: TGroupBox;
    grpPWtype: TGroupBox;
    grpSyncProfile: TGroupBox;
    lblSelectedProfile: TLabel;
    lblCpyA1: TLabel;
    lblExtDrivePath: TLabel;
    lblCpyA: TLabel;
    lblContN1: TLabel;
    lblCpy1: TLabel;
    lblCpy2: TLabel;
    lblCpy3: TLabel;
    lblContN2: TLabel;
    lblContN3: TLabel;
    lblContN4: TLabel;
    lblContN5: TLabel;
    lblContN6: TLabel;
    lblContN7: TLabel;
    lblContN8: TLabel;
    lblContN9: TLabel;
    lblPWtype1: TLabel;
    lblPWtype2: TLabel;
    memUnisonCfg: TMemo;
    pnlTopButtons: TPanel;
    pctrlConfig: TPageControl;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    btnClrEdt1: TSpeedButton;
    txtLastExtraSyncTime: TStaticText;
    tsContainerSync: TTabSheet;
    tsExtraSync: TTabSheet;
    tsGeneral: TTabSheet;
    XMLPropStorage1: TXMLPropStorage;
    procedure btnClrEdtExtraClick(Sender: TObject);
    procedure btnClrEdtClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnExtraSyncClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure chkAutoHideChange(Sender: TObject);
    procedure chkExtraSyncOnChange(Sender: TObject);
    procedure chkHideOnFocusLostChange(Sender: TObject);
    procedure chkStartMinimizedClick(Sender: TObject);
    procedure chkStayOnTopChange(Sender: TObject);
    procedure CloseButtonClick(Sender: TObject);
    procedure edtContChange(Sender: TObject);
    procedure edtContClick(Sender: TObject);
    procedure edtContKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure edtEditingDone(Sender: TObject);
    procedure edtExtDrivePathClick(Sender: TObject);
    procedure edtSynProfileChange(Sender: TObject);
    procedure edtExtraMouseEnter(Sender: TObject);
    procedure edtExtraClick(Sender: TObject);
    procedure edtExtraChange(Sender: TObject);
    procedure edtExtraEnter(Sender: TObject);
    procedure edtExtraExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure pctrlConfigChange(Sender: TObject);
    procedure tsExtraSyncEnter(Sender: TObject);
    procedure tsContainerSyncEnter(Sender: TObject);
    procedure tsExtraSyncExit(Sender: TObject);
  private
    procedure cmbSyncSelUpd;

  public

  end;


type
  Container = record             // container properties
    Name: string;                // the Name of the Container, Caption of container-button
    //NameEnabled: boolean;
    FileName: string;            // the encrypted container file name
    Directory: string;           // the decrypted container directory, drive content
    PwType: integer;
    // the Type Index of used Password (two kinds are possible)
    PwTypeName: string;          // the Type Name of used Password
    SyncProfile: string;         // the unison profile file.
    //SyncName: string;
    //SyncNameEnabled: boolean;
    //SyncFileName: string;
    //SyncDirectory: string;
    LastSyncTimeStamp: string;
    LastSyncMessage: string;
  end;

var
  frmConfig: TfrmConfig;
  Containers: array[1..19] of Container;
  fileonmedia: string; //own file name on removable media with path

// return the zCrypt Drive Path from cZcrypt Container file name
function zCdrivePath(zCfileName: string): string;


implementation

uses main;

{$R *.lfm}

{ TfrmConfig }

// return the zCrypt Drive Path from cZcrypt Container file name
function zCdrivePath(zCfileName: string): string;
var
  fext: string;
  zCmountPath: string;
  PwDBPath: string;
  rstring: string;

begin
  Result := '';
  fext := ExtractFileExt(zCfileName);
  zCmountPath := '/run/media/private/' + GetEnvironmentVariable('USER') + '/';
  PwDBPath := '';

  if zCfileName <> '' then
  begin
    if fext <> '' then
      rstring := zCmountPath + ExtractFileNameWithoutExt(zCfileName) + '/'
    else
      rstring := zCmountPath + ExtractFileName(zCfileName) + '/';
  end;
  Result := rstring;
end;


// Calculate Icon and Text Position for Container Buttons
procedure TfrmConfig.cmbSyncSelUpd;
var
  i, ix: integer;
  NameStr: string;
  NameStrLen: integer;
  MaxContNameLen: integer; //check max length for Container Names
  MaxContNameStr: string;
  CbtnMargin: integer; //calculated left margin for main form container buttons
  CbtnSpacing: integer; //calculated spacing btw. glyph and text for container btns
  btnwidth: integer;
  imgwidth: integer;
  fntwidth: integer;

begin
  ix := 0;
  MaxContNameLen := 0;
  MaxContNameStr := '';

  btnwidth := frmMain.btnOpenC1.Width;
  imgwidth := frmMain.imglstContBtn.Width;
  CbtnMargin := 5;

  try
    // Calculate BitButton Captions Spacing
    // depending on Container Names Length

    for i := 1 to 19 do
    begin
      if i <> 10 then
      begin
        NameStr := Containers[i].Name;
        NameStrLen := Length(NameStr);
        if NameStrLen > MaxContNameLen then
        begin
          MaxContNameLen := NameStrLen;
          MaxContNameStr := NameStr;
        end;
        fntwidth := frmMain.btnOpenC1.Font.GetTextWidth(MaxContNameStr);

        CbtnSpacing := Trunc((btnwidth - fntwidth * MaxContNameLen - imgwidth - 30) / 2);
        if CbtnSpacing < 3 then
          CbtnSpacing := 3;

        //CbtnSpacing := Trunc((btnwidth - fntwidth * MaxContNameLen - imgwidth - 5) / 2);
        //if CbtnSpacing < 7 then
        //  CbtnSpacing := 7;
      end;
    end;

  except
    on E: Exception do
      ShowMessage('Error_219 in config module: ' + E.ClassName + #13#10 + E.Message);
  end;
  // Apply calculated BitButton Spacings on frmMain
  try
    for i := 0 to frmMain.ComponentCount - 1 do
    begin
      ix := frmMain.Components[i].Tag;
      if InRange(ix, 1, 19) and (ix <> 10) then
      begin
        TBitBtn(frmMain.Components[i]).Spacing := CbtnSpacing;
        TBitBtn(frmMain.Components[i]).Margin := CbtnMargin;
      end;
    end;
  except
    on E: Exception do
      ShowMessage('Error_235 in config module: ' + E.ClassName + #13#10 + E.Message);
  end;
end;


procedure TfrmConfig.FormCreate(Sender: TObject);
var
  i, i_pw: integer;
  pwtyp_: integer;
  cname: string;

  ownapp: string; //own running file name with path

begin
  XMLPropStorage1.FileName := ExtractFilePath(ParamStr(0)) + '.' +
    ExtractFileName(ParamStr(0));

  //initialize Containers Data Array
  for i := 1 to 19 do
  begin
    Containers[i].Directory := '';
    Containers[i].FileName := '';
    Containers[i].Name := '';
    //Containers[i].NameEnabled := False;
    Containers[i].PwType := 1;
    Containers[i].PwTypeName := '---';
    Containers[i].SyncProfile := '';
  end;

  //self.BorderIcons := [biSystemMenu]; //enable only close button on Form Border
  self.BorderIcons := [];
  XMLPropStorage1.Restore;



  for i := 1 to 9 do
  begin // create item texts for PWtype Comboboxes
    (FindComponent('cmbPwTypC' + IntToStr(i)) as TComboBox).Items.Clear;
    (FindComponent('cmbPwTypC' + IntToStr(i)) as TComboBox).Items.Add(
      edtPWtypName1.Text);
    (FindComponent('cmbPwTypC' + IntToStr(i)) as TComboBox).Items.Add(
      edtPWtypName2.Text);
  end;

  if chkFirstRun.Checked then
  begin
    chkFirstRun.Checked := False;

    frmConfig.Show;
    frmConfig.BringToFront;
    frmConfig.Activate;
    ShowMessage('First Start or New Program version Detected. Please Check Configuration or Configure zcryptui first.');
  end;

  for i := 1 to 19 do
  begin
    if i <> 10 then
    begin
      // processing Container Names
      Containers[i].Name := ExtractFileNameWithoutExt(ExtractFileName(
        (FindComponent('edtCont' + IntToStr(i)) as TFileNameEdit).Filename));
      cname := Containers[i].Name;
      //(FindComponent('edtContName' + IntToStr(i)) as TEdit).Text := cname;
      (frmMain.FindComponent('btnOpenC' + IntToStr(i)) as TBitBtn).Caption := cname;

      // processing Container Filenames
      Containers[i].FileName :=
        (FindComponent('edtCont' + IntToStr(i)) as TFileNameEdit).FileName;

      // processing Container zCdrivePath-s
      Containers[i].Directory := zCdrivePath(Containers[i].FileName);

      if i > 10 then
      begin
        i_pw := i - 10;
        // fill default initial directory in fileedits for external sync containers
        (FindComponent('edtCont' + IntToStr(i)) as TFileNameEdit).InitialDir :=
          edtExtDrivePath.Directory;
      end
      else
        i_pw := i;

      // processing Password Types
      pwtyp_ := (FindComponent('cmbPwTypC' + IntToStr(i_pw)) as
        TComboBox).ItemIndex + 1;
      if not InRange(pwtyp_, 1, 2) then
        pwtyp_ := 2;

      Containers[i].PwType := pwtyp_;
      Containers[i].PwTypeName :=
        (FindComponent('edtPWtypName' + IntToStr(pwtyp_)) as TEdit).Text;
    end;
  end;

  cmbSyncSelUpd;

  if frmMain.pop_alwaysOnTop.Checked = True then
    frmMain.FormStyle := fsSystemStayOnTop
  else
    frmMain.FormStyle := fsNormal;

  grpExtraSync.Enabled := chkExtraSyncOn.Checked;

  frmMain.Height := frmMain.btnUmountSyncDrives.Top +
    frmMain.btnUmountSyncDrives.Height + 4;
  frmMain.uiNormalWidth := frmMain.btnC9eject.Left + frmMain.btnC9eject.Width + 4;
  frmMain.uiExtWidth := frmMain.btnUmountSyncDrives.Left +
    frmMain.btnUmountSyncDrives.Width + 4;

  pctrlConfig.TabIndex := 0;

  //check if newer zcryptui file exists on removable media
  fileonmedia := edtExtDrivePath.Directory + '/' + ExtractFileName(ParamStr(0));
  ownapp := ExtractFilePath(ParamStr(0)) + ExtractFileName(ParamStr(0));

  //ShowMessage('own app: ' + DateTimeToStr(FileDateToDateTime(FileAge(ownapp))) +
  //  #13 + 'file on media: ' + DateTimeToStr(FileDateToDateTime(FileAge(fileonmedia))));

  if FileExists(fileonmedia) then
  begin
    //if file on media newer than own app timestamp
    if FileAge(fileonmedia) > FileAge(ownapp) then
      ShowMessage('Newer Program Version Recognized' + #13 + 'on ' +
        edtExtDrivePath.Directory);
  end;

end; //Create

procedure TfrmConfig.OKButtonClick(Sender: TObject);
var
  i, i_pw: integer;
  pwtyp_: integer;
  cname: string;

begin
  cmbSyncSelUpd;
  chkFirstRun.Checked := False;
  XMLPropStorage1.Save;

  for i := 1 to 19 do
  begin
    if i <> 10 then
    begin
      // processing Container Names
      Containers[i].Name := ExtractFileNameWithoutExt(ExtractFileName(
        (FindComponent('edtCont' + IntToStr(i)) as TFileNameEdit).Filename));
      cname := Containers[i].Name;
      //(FindComponent('edtContName' + IntToStr(i)) as TEdit).Text := cname;
      (frmMain.FindComponent('btnOpenC' + IntToStr(i)) as TBitBtn).Caption := cname;

      // processing Container Filenames
      Containers[i].FileName :=
        (FindComponent('edtCont' + IntToStr(i)) as TFileNameEdit).FileName;

      // processing Container zCdrivePath-s
      Containers[i].Directory := zCdrivePath(Containers[i].FileName);

      if i > 10 then
        i_pw := i - 10
      else
        i_pw := i;
      // processing Password Types
      pwtyp_ := (FindComponent('cmbPwTypC' + IntToStr(i_pw)) as
        TComboBox).ItemIndex + 1;
      if not InRange(pwtyp_, 1, 2) then
        pwtyp_ := 2;
      Containers[i].PwType := pwtyp_;
      Containers[i].PwTypeName :=
        (FindComponent('edtPWtypName' + IntToStr(pwtyp_)) as TEdit).Text;
    end;
  end;
  frmConfig.Close;
end;

procedure TfrmConfig.pctrlConfigChange(Sender: TObject);
begin
  try
    frmConfig.ActiveControl := nil;

  finally
  end;
end;

procedure TfrmConfig.tsExtraSyncEnter(Sender: TObject);
var
  i: integer;
  fncomp: TFileNameEdit;
  btncomp: TSpeedButton;
begin
  try
    Shape1.Visible := False;
    Shape2.Visible := False;
    Shape3.Visible := False;
    for i := 1 to 3 do
    begin
      // show text in filename-edits right aligned
      // processing Container Names
      fncomp := (FindComponent('edtExtra' + IntToStr(i)) as TFileNameEdit);
      fncomp.SelStart := Length(fncomp.Text);
      // ... and activate clear buttons for filenameedits
      btncomp := (FindComponent('btnClrEdt' + IntToStr(i + 20)) as TSpeedButton);
      btncomp.Enabled := main.IntToBool(fncomp.SelStart);
      if Length(edtExtra1.Filename) > 0 then
      begin
        Shape2.Visible := False;
        Shape3.Visible := False;
        Shape1.Visible := True;
        memUnisonCfg.Lines.LoadFromFile(edtExtra1.Filename);
      end
      else
      begin
        Shape1.Visible := False;
        Shape3.Visible := False;
        if Length(edtExtra2.Filename) > 0 then
        begin
          Shape2.Visible := True;
          memUnisonCfg.Lines.LoadFromFile(edtExtra2.Filename);
        end
        else
        begin
          Shape1.Visible := False;
          Shape2.Visible := False;
          if Length(edtExtra3.Filename) > 0 then
          begin
            Shape3.Visible := True;
            memUnisonCfg.Lines.LoadFromFile(edtExtra3.Filename);
          end;
        end;
      end;

    end;

  finally
  end;
end;

procedure TfrmConfig.tsContainerSyncEnter(Sender: TObject);
var
  i: integer;
  fncomp: TFileNameEdit;
  btncomp: TSpeedButton;

begin
  try
    for i := 1 to 9 do // processing component names...
    begin
      // show text in Container filename-edits 1-9 right aligned
      // processing Container Names
      fncomp := (FindComponent('edtCont' + IntToStr(i)) as TFileNameEdit);
      fncomp.SelStart := Length(fncomp.Text);
      // ...and activate clear buttons 1-9 for filenameedits
      btncomp := (FindComponent('btnClrEdt' + IntToStr(i)) as TSpeedButton);
      btncomp.Enabled := main.IntToBool(fncomp.SelStart);

      // show text in Container filename-edits 11-19 right aligned
      // processing Container Names
      fncomp := (FindComponent('edtCont' + IntToStr(i + 10)) as TFileNameEdit);
      fncomp.SelStart := Length(fncomp.Text);
      // ...and activate clear buttons 1-9 for filenameedits
      btncomp := (FindComponent('btnClrEdt' + IntToStr(i + 10)) as TSpeedButton);
      btncomp.Enabled := main.IntToBool(fncomp.SelStart);

      // show text in filename-edits 1-9 for Profiles right aligned
      fncomp := (FindComponent('edtSynProfile' + IntToStr(i)) as TFileNameEdit);
      fncomp.SelStart := Length(fncomp.Text);
    end;
    edtExtDrivePath.SelStart := Length(edtExtDrivePath.Text);

  finally
  end;
end;

procedure TfrmConfig.tsExtraSyncExit(Sender: TObject);
begin
  //mark Extra Sync unavailable if nothing configured
  if edtExtra1.Text + edtExtra2.Text + edtExtra3.Text = '' then
    chkExtraSyncOn.Checked := False;
end;

procedure TfrmConfig.btnExtraSyncClick(Sender: TObject);
var
  i: integer;
  outp: string;
  syncprofilefile: string; //whole sync profile file name
  syncprofilename: string; //sync profile name
  formattedparams: string; //sync profile name, formatted for command string
  procUnison: TProcess; //external Process for Unison
  exitstatus: integer;
  LastExtraSyncTime: string; //Last Extra Sync Time String

begin
  btnExtraSync.Enabled := False;
  //get caller Sync-Speedbutton Tag
  try
    for i := 1 to 3 do
    begin
      //get whole sync profile file name
      syncprofilefile := TFileNameEdit(FindComponent('edtExtra' + IntToStr(i))).Filename;
      //get unison profile file name
      syncprofilename := ExtractFileNameOnly(syncprofilefile);

      if FileExists(syncprofilefile) then
      begin
        //format sync profile name and params for RunCommand
        formattedparams := ' ''' + syncprofilename + '''';
        //create Unison cli process
        procUnison := TProcess.Create(nil);
        try
          //run unison cli process
          procUnison.Executable := 'unison';
          procUnison.Parameters.Add(syncprofilename);
          procUnison.Execute;
          while procUnison.ExitStatus < 0 do
          begin //use this instead of process.Option poWaitOnExit
            //wait until last process ended
            Application.ProcessMessages;
          end;

          //unison exit status:
          //0 = everything is ok
          //1 = something fails
          exitstatus := procUnison.ExitStatus;
          procUnison.Free;

          //check if unison procUnison exited with status code gt 0
          //means: if anything while sync is skipped or failed
          if exitstatus > 0 then
          begin
            //The replicas are not fully synchronized. There was skipped item(s).
            ShowMessage(
              'Extra-Sync break, check the following:' + #13 +
              ' - the directory that Unison wants to synchronize is available' +
              #13 + ' - the drive that Unison wants to synchronize is mounted' +
              #13 +
              ' - Unison has skipped items in replicas, data may have been modified in different locations since the last synchronization.'
              +
              #13 + '   ' + #13 +
              'Check next your Unison configuration files (~/.unison/*.prf) for Extra Sync,' + #13 +
              'and Start Unison with the graphical interface (Unison-gtk) to get detailed informations about the reasons.'
              +
              #13 + '   ' + #13 +
              'Configured Extra Synchronisation was not executed.');
            {
            Application.ProcessMessages;
            // Unison-GTK Variant will be started
            if not AppIsRunning('unison-gtk') then
              if RunCommand('unison-gtk' + formattedparams, outp) then
              begin
                LastExtraSyncTime := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
                btnExtraSync.Hint :=
                  'Extra Sync Operations' + #13 +
                  '(Last Extra Sync Time: ' + LastExtraSyncTime + ')';
                txtLastExtraSyncTime.Caption :=
                  'Last Extra Sync Time: ' + LastExtraSyncTime;
              end
              else
              begin
                if MessageDlg('zcryptui: Unison Error',
                  'Unison Error occured. Check if the Unison software is installed correctly.',
                  mtWarning, [mbYes], 0) = mrYes then
                ;
              end;
            //Memo1.Lines.Append('unison-gtk is finished.');
            }
          end
          else
          begin
            //Memo1.Lines.Append('unison background task is finished correctly.');
            LastExtraSyncTime := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
            btnExtraSync.Hint :=
              'Extra Sync Operations' + #13 + '(Last Extra Sync Time: ' +
              LastExtraSyncTime + ')';
            txtLastExtraSyncTime.Caption := 'Last Extra Sync Time: ' + LastExtraSyncTime;
          end;
        except
          procUnison.Free;
          //Memo1.Lines.Append('Process Exception occured: ?');
          ShowMessage('Unison Background Process Exception occured');
          btnExtraSync.Enabled := True;
        end;
      end; //if FileExists(syncprofilefile)
    end; //for i := 1 to 3 do
  except
    if frmConfig.Showing and (pctrlConfig.ActivePage = tsExtraSync) then
      ShowMessage('Extra Sync ERROR');
  end;
  if frmConfig.Showing and (pctrlConfig.ActivePage = tsExtraSync) then
    ShowMessage('Extra Sync finised');

  //run OS-Sync command in terminal to write all data to data storage...
  //Regular Container OS-Sync Function
  //frmMain.pnlSyncInfo.Caption := 'Write Data...';
  if (DirectoryExists(edtExtDrivePath.Directory)) then
    if frmMain.OSSyncStorage(edtExtDrivePath.Directory) = 0 then;
  //Application.ProcessMessages;

  btnExtraSync.Enabled := True;
end;  //btnExtraSyncClick

procedure TfrmConfig.CancelButtonClick(Sender: TObject);
begin
  XMLPropStorage1.Restore;
  frmConfig.Close;
end;

procedure TfrmConfig.chkAutoHideChange(Sender: TObject);
begin
  //Lock configuration checkboxes [Hide App on Idle] and [Hide App on Focus lost] against each other
  if chkAutoHide.Checked then
  begin
    chkHideOnFocusLost.Checked := False;
    chkHideOnFocusLost.Enabled := False;
  end
  else
  begin
    chkHideOnFocusLost.Enabled := True;
  end;
end;

procedure TfrmConfig.chkExtraSyncOnChange(Sender: TObject);
begin
  grpExtraSync.Enabled := chkExtraSyncOn.Checked;
end;

procedure TfrmConfig.chkHideOnFocusLostChange(Sender: TObject);
begin
  //Stay on Top is not possible, if HideOnFocusLost selected
  //Lock configuration checkboxes [Hide App on Idle] and [Hide App on Focus lost] against each other
  if chkHideOnFocusLost.Checked then
  begin
    chkStayOnTop.Checked := False;
    chkStayOnTop.Enabled := False;
    chkAutoHide.Checked := False;
    chkAutoHide.Enabled := False;
  end
  else
  begin
    chkStayOnTop.Enabled := True;
    chkAutoHide.Enabled := True;
  end;
end;

procedure TfrmConfig.btnClrEdtExtraClick(Sender: TObject);
begin
  // clear FileEdit Fields in Config - Additional Operations
  (FindComponent('edtExtra' + IntToStr((Sender as TSpeedButton).Tag - 20)) as
    TFileNameEdit).Clear;
end;

procedure TfrmConfig.btnClrEdtClick(Sender: TObject);
begin
  // clear FileEdit Fields in Config - Container Synchronization
  (FindComponent('edtCont' + IntToStr((Sender as TSpeedButton).Tag)) as
    TFileNameEdit).Clear;
end;

procedure TfrmConfig.btnInfoClick(Sender: TObject);
begin
  //frmMain.SplashAbout1.ShowAbout;
end;

procedure TfrmConfig.chkStartMinimizedClick(Sender: TObject);
begin
  //set property in popupmenu too
  frmMain.pop_startMinimized.Checked := chkStartMinimized.Checked;
end;

procedure TfrmConfig.chkStayOnTopChange(Sender: TObject);
begin
  //HideOnFocusLost is not possible, if Stay on Top selected
  if chkStayOnTop.Checked then
  begin
    chkHideOnFocusLost.Checked := False;
    chkHideOnFocusLost.Enabled := False;
  end
  else
  begin
    chkHideOnFocusLost.Enabled := True;
  end;




  //set property in popupmenu too
  frmMain.pop_alwaysOnTop.Checked := chkStayOnTop.Checked;

  if frmMain.pop_alwaysOnTop.Checked then
    frmMain.FormStyle := fsSystemStayOnTop
  else
    frmMain.FormStyle := fsNormal;
end;

procedure TfrmConfig.CloseButtonClick(Sender: TObject);
begin
  frmConfig.Close;
end;

procedure TfrmConfig.edtContChange(Sender: TObject);
begin
  try
    (FindComponent('btnClrEdt' + IntToStr((Sender as TFileNameEdit).Tag))
      as TSpeedButton).Enabled := main.IntToBool(Length((Sender as TFileNameEdit).Text));

  finally
  end;
end;

procedure TfrmConfig.edtContClick(Sender: TObject);
begin
  (Sender as TFileNameEdit).SelStart := Length((Sender as TFileNameEdit).Text);
end;

procedure TfrmConfig.edtContKeyDown(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  try
    // jump to next field with ENTER Key in Container Configuration Form
    if Key = VK_RETURN then // 13 =Enter
    begin
      Key := 0; // Beep verhindern.
      Screen.ActiveForm.SelectNext(Screen.GetCurrentModalForm.ActiveControl, True, True);
    end;

  finally
  end;
end;

procedure TfrmConfig.edtEditingDone(Sender: TObject);
var
  cmpnr: integer;
  fnstring, leftfnstring: string;

begin
  // identify Sender
  cmpnr := (Sender as TFileNameEdit).Tag;
  // left side of file directory must match configured external drive path
  fnstring := (Sender as TFileNameEdit).FileName;
  leftfnstring := LeftStr(fnstring, Length(edtExtDrivePath.Directory));

  if leftfnstring = edtExtDrivePath.Directory then
  begin
    // create encrypted container files location without drive path
    //(Sender as TFileNameEdit).Text :=
    //  ReplaceStr(fnstring, edtExtDrivePath.Directory, '');
    // assign Container Name Record
    Containers[cmpnr].Name := ExtractFileNameWithoutExt(ExtractFileName(
      (Sender as TFileNameEdit).FileName));
    (Sender as TFileNameEdit).BiDiMode := bdRightToLeft;
    (Sender as TFileNameEdit).Alignment := taRightJustify;
    (Sender as TFileNameEdit).Refresh;
  end
  else  // selected file does not match external drive path
  begin
    (Sender as TFileNameEdit).InitialDir := edtExtDrivePath.Directory;
    if fnstring <> '' then
    begin
      ShowMessage('the selected File is not in drive "' +
        edtExtDrivePath.Directory + '"');
      (Sender as TFileNameEdit).Text := '';
      //(Sender as TFileNameEdit).FileName := '';
    end;
  end;
  cmbSyncSelUpd;
end;

procedure TfrmConfig.edtExtDrivePathClick(Sender: TObject);
begin
  (Sender as TDirectoryEdit).SelStart := Length((Sender as TDirectoryEdit).Text);
end;


procedure TfrmConfig.edtSynProfileChange(Sender: TObject);
var
  cmpnr: integer;
begin
  // identify Sender
  cmpnr := (Sender as TFileNameEdit).Tag - 20;
  // assign Container Name Record
  Containers[cmpnr].SyncProfile := (Sender as TFileNameEdit).FileName;
end;

procedure TfrmConfig.edtExtraMouseEnter(Sender: TObject);
var
  i: integer;
  sedt: string;
  cedt: TFileNameEdit;
begin
  // put cursor to the end of Filename-String in TFileNameEdit
  i := (Sender as TFileNameEdit).Tag;
  sedt := 'edtExtra' + IntToStr(i);
  cedt := ((FindComponent(sedt)) as TFileNameEdit);
  cedt.SelStart := Length(cedt.Text);
end;

procedure TfrmConfig.edtExtraClick(Sender: TObject);
begin
  (Sender as TFileNameEdit).SelStart := Length((Sender as TFileNameEdit).Text);
end;


procedure TfrmConfig.edtExtraEnter(Sender: TObject);
var
  i: integer;
begin
  // make corresponding Shape visible
  i := (Sender as TFileNameEdit).Tag;
  lblSelectedProfile.Caption := IntToStr(i);
  ((FindComponent('Shape' + IntToStr(i))) as TShape).Visible := True;
  // handle unison profile preview and profile filename
  try
    if FileExists((Sender as TFileNameEdit).FileName) then
      memUnisonCfg.Lines.LoadFromFile((Sender as TFileNameEdit).FileName);
  except
    memUnisonCfg.Lines.Clear;
    (Sender as TFileNameEdit).Clear;
  end;
  if Length((Sender as TFileNameEdit).FileName) = 0 then
    memUnisonCfg.Lines.Clear;
end;

procedure TfrmConfig.edtExtraChange(Sender: TObject);
begin
  try
    if FileExists((Sender as TFileNameEdit).FileName) then
      memUnisonCfg.Lines.LoadFromFile((Sender as TFileNameEdit).FileName);
  except
    memUnisonCfg.Lines.Clear;
    (Sender as TFileNameEdit).Clear;
  end;
  (FindComponent('btnClrEdt' + IntToStr((Sender as TFileNameEdit).Tag + 20))
    as TSpeedButton).Enabled := main.IntToBool(Length((Sender as TFileNameEdit).Text));
  if Length((Sender as TFileNameEdit).FileName) = 0 then
    memUnisonCfg.Lines.Clear;
end;

procedure TfrmConfig.edtExtraExit(Sender: TObject);
var
  i: integer;
begin
  try
    // make corresponding Shape invisible
    i := (Sender as TFileNameEdit).Tag;
    ((FindComponent('Shape' + IntToStr(i))) as TShape).Visible := False;
    // handle unison profile preview and profile filename
    try
      if FileExists((Sender as TFileNameEdit).FileName) then
        memUnisonCfg.Lines.LoadFromFile((Sender as TFileNameEdit).FileName);
    except
      memUnisonCfg.Lines.Clear;
      (Sender as TFileNameEdit).Clear;
    end;

  finally
  end;
end;

end.
