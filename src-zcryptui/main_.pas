{
 zCryptUI Main form

 easy to use User Interface to Handle VeraCrypt Container with zuluCrypt-cli
 and synchronizing Data with Unison.

 zcryptui is a user interface to use encrypted veracrypt containers with zuluCrypt-cli. 
 The focus is on ease of use to handle of encrypted containers, in contrast to the VeraCrypt GUI.
 zcryptui is a tool written in Free Pascal / Lazarus and enables a practical and easy use of Veracrypt.

 (C) 2021 Janos Wunderlich 
 This source is free software. You can redistribute it and/or modify it under the terms of the 
 GNU General Public License as published by the Free Software Foundation.
 
 zCryptUI is made to increase productivity at the using VeraCrypt Containers.

 Functions:
 - Reasonable, easy to use UI for encrypted VeraCrypt Data Containers
 - uses zuluCrypt-cli to handle VC Containers
 - uses Unison to synchronize Data between VC Containers
 - keeps temporarily VC Passwords in Memory with AES Encryption
 - configurable password flushing
 - Configurable UI behavior with additional Tray Icon and Popup Menu
 - Extended Configuration for VC and (External) Sync Containers

 Security:
 Password Encryption with Rijndael encryption algorithm
 and randomly generated Passphrase to protect temporarly saved passwords for
 Encrypted Data Container (VeraCrypt).

 used libraries:
 DCPcrypt Cryptographic Component Library:
  https://sourceforge.net/projects/lazarus-ccr/files/DCPcrypt/
 open icon library:
  https://sourceforge.net/projects/openiconlibrary/
 UniqueInstance component Copyright (C) 2006 Luiz Americo Pereira Camara

 !!!
 Use zuluCrypt packages >= V.6.0 directly from http://mhogomchungu.github.io/zuluCrypt/
 !!!
 zuluCrypt packages from developer page are better than those provided by these distributions
 because of the following reasons:
 1. packages from developer page do not generates a polkit prompt that requires a root password
    when GUI components are started.
 2. packages from developer page provides CLI components that work from a normal user account.

}

unit main;

// {$mode objfpc}{$H+}
// https://www.freepascal.org/docs-html/prog/progap4.html#progse62.html
{$mode delphi}

interface

uses
  {$IFDEF UNIX} unix,{$ENDIF}
  {$IFDEF WINDOWS} Windows,{$ENDIF}
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  config, prequest, zcwait, Buttons, Menus, XMLPropStorage, ComCtrls,
  UniqueInstance, uPoweredby, JvLED, JvJanLED, DCPice, Process, uappisrunning,
  FileUtil, LCLType, LCLIntf, EditBtn, Math, dateutils, LazFileUtils,
  usplashabout, DCPsha256, DCPrijndael, vinfo, tool1, fptimer, TypInfo, uMsgDlg,
  synclog;

//{$R data.rc}
//not working with ARM Target (Raspberry)
//use instead: Project->Options->Resources
//https://wiki.freepascal.org/IDE_Window:_Project_Options#Resources

type

  { TfrmMain }

  TfrmMain = class(TForm)
    ApplicationProperties1: TApplicationProperties;
    btnC11eject: TSpeedButton;
    btnC12eject: TSpeedButton;
    btnC13eject: TSpeedButton;
    btnC14eject: TSpeedButton;
    btnC15eject: TSpeedButton;
    btnC16eject: TSpeedButton;
    btnC17eject: TSpeedButton;
    btnC18eject: TSpeedButton;
    btnC19eject: TSpeedButton;
    btnUmountSyncDrives: TSpeedButton;
    btnShowLog: TSpeedButton;
    btnOpenC1: TBitBtn;
    btnOpenC11: TBitBtn;
    btnOpenC7: TBitBtn;
    btnOpenC17: TBitBtn;
    btnOpenC3: TBitBtn;
    btnOpenC13: TBitBtn;
    btnOpenC4: TBitBtn;
    btnOpenC14: TBitBtn;
    btnOpenC8: TBitBtn;
    btnOpenC18: TBitBtn;
    btnOpenC6: TBitBtn;
    btnOpenC16: TBitBtn;
    btnOpenC9: TBitBtn;
    btnOpenC19: TBitBtn;
    btnOpenC5: TBitBtn;
    btnOpenC15: TBitBtn;
    btnOpenC2: TBitBtn;
    btnOpenC12: TBitBtn;
    btnC8eject: TSpeedButton;
    btnC4eject: TSpeedButton;
    btnC7eject: TSpeedButton;
    btnExtendUi: TSpeedButton;
    btnC9eject: TSpeedButton;
    btnC5eject: TSpeedButton;
    btnC3eject: TSpeedButton;
    btnC6eject: TSpeedButton;
    btnC1eject: TSpeedButton;
    btnSyncAll: TSpeedButton;
    btnSyncC1: TSpeedButton;
    btnSyncC2: TSpeedButton;
    btnSyncC3: TSpeedButton;
    btnSyncC4: TSpeedButton;
    btnSyncC5: TSpeedButton;
    btnSyncC6: TSpeedButton;
    btnSyncC7: TSpeedButton;
    btnSyncC8: TSpeedButton;
    btnSyncC9: TSpeedButton;
    btnUmountAll: TSpeedButton;
    Poweredby1: TPoweredby;
    btnInfo: TSpeedButton;
    TrayIcon1: TTrayIcon;
    tPeriodicIdleTimer: TIdleTimer;
    tInactivity: TIdleTimer;
    imglstGeneral: TImageList;
    pnlSyncState1: TPanel;
    pnlSyncState2: TPanel;
    pnlSyncState3: TPanel;
    pnlSyncState4: TPanel;
    pnlSyncState5: TPanel;
    pnlSyncState6: TPanel;
    pnlSyncState7: TPanel;
    pnlSyncState8: TPanel;
    pnlSyncState9: TPanel;
    txtNewer11: TStaticText;
    txtNewer12: TStaticText;
    txtNewer13: TStaticText;
    txtNewer14: TStaticText;
    txtNewer15: TStaticText;
    txtNewer16: TStaticText;
    txtNewer17: TStaticText;
    txtNewer18: TStaticText;
    txtNewer19: TStaticText;
    txtNewer2: TStaticText;
    txtNewer3: TStaticText;
    txtNewer4: TStaticText;
    txtNewer5: TStaticText;
    txtNewer6: TStaticText;
    txtNewer7: TStaticText;
    txtNewer8: TStaticText;
    txtNewer9: TStaticText;
    txtOpen1: TStaticText;
    txtNewer1: TStaticText;
    txtOpen11: TStaticText;
    txtOpen12: TStaticText;
    txtOpen13: TStaticText;
    txtOpen14: TStaticText;
    txtOpen15: TStaticText;
    txtOpen16: TStaticText;
    txtOpen17: TStaticText;
    txtOpen18: TStaticText;
    txtOpen19: TStaticText;
    txtOpen2: TStaticText;
    txtOpen4: TStaticText;
    txtOpen3: TStaticText;
    txtOpen8: TStaticText;
    txtOpen7: TStaticText;
    txtOpen6: TStaticText;
    txtOpen5: TStaticText;
    txtOpen9: TStaticText;
    tBlink: TIdleTimer;
    tSyncDone: TIdleTimer;
    imglstContBtn: TImageList;
    pnlHostInfo: TPanel;
    pnlSyncInfo: TPanel;
    PopupMenu1: TPopupMenu;
    pop_alwaysOnTop: TMenuItem;
    pop_Exit: TMenuItem;
    pop_ShowHide: TMenuItem;
    pop_startMinimized: TMenuItem;
    btnC2eject: TSpeedButton;
    btnCfg: TSpeedButton;
    btnFlush: TSpeedButton;
    SplashAbout1: TSplashAbout;
    Splitter1: TSplitter;
    zcryptui_mutex_tmp: TUniqueInstance;
    XMLPropStorage1: TXMLPropStorage;
    procedure ApplicationProperties1Deactivate(Sender: TObject);
    procedure ApplicationProperties1UserInput(Sender: TObject; Msg: cardinal);
    procedure btnCfgClick(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnUmountSyncDrivesClick(Sender: TObject);
    procedure btnExtendUiClick(Sender: TObject);
    procedure btnFlushClick(Sender: TObject);
    procedure btnShowLogClick(Sender: TObject);
    procedure btnOpenContainerClick(Sender: TObject);
    procedure btnContainerEjectClick(Sender: TObject);
    procedure btnUmountAllClick(Sender: TObject);
    procedure btnSyncAllClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure tPeriodicIdleTimerTimer(Sender: TObject);
    procedure btnSyncContainerClick(Sender: TObject);
    procedure PopupMenu1Close(Sender: TObject);
    procedure pop_alwaysOnTopClick(Sender: TObject);
    procedure pop_ExitClick(Sender: TObject);
    procedure pop_ShowHideClick(Sender: TObject);
    procedure pop_startMinimizedClick(Sender: TObject);
    procedure tBlinkTimer(Sender: TObject);
    procedure tInactivityTimer(Sender: TObject);
    procedure TrayIcon1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure tSyncDoneTimer(Sender: TObject);
    procedure zcryptui_mutex_tmpOtherInstance(Sender: TObject;
      ParamCount: integer; const Parameters: array of string);
    function EncryptPWStr(DecryptedPwStr: string): string;
    function DecryptPWStr(EncryptedPwStr: string): string;
    //writes any data buffered in memory out to disk
    function OSSyncStorage(DirPath: string): integer;

    //set Font in App at Runtime
    procedure SetFontProperties(Control: TControl; FntName: TFontName;
      Size: integer; Styles: TFontStyles);

    function UnisonLogSummary: string; //returns the last log line from unison.log

  private
    //tBlink: TFPTimer;
    //procedure tBlinkExec(Sender: TObject);

  public

  var
    syncdriveispresent: boolean; //Flag for external Container / Drive connected
    uiExtWidth, uiNormalWidth: integer; //form Widths
    uiSmallOnce: boolean; //Collapse Ui once after removing ext/sync media
    //Semaphore for Sync/Batch Sync active, need for setting checked symbol:
    syncDone: boolean;
    SyncPairsCount: integer;  //count mounted sync containers
    MsgDlgEx: TMsgDlgEx;  //Auto Closing Message
    MessageStr: string; //Message String for Auto Closing Message
  end;

//https://wiki.freepascal.org/Executing_External_Programs
type
  { TProcessTimeout }
  TProcessTimeout = class(TProcess)
  public
    timeoutperiod: TTime;
    timedout: boolean;
    started: TDateTime;
    procedure LocalnIdleSleep(Sender, Context: TObject;
      status: TRunCommandEventCode; const message: string);
  end;

var
  frmMain: TfrmMain;
  OutSide: boolean; //if true, the Program works in VirtualBox outside from Home!
  zCguiExists: boolean; //true, if file /usr/bin/zuluCrypt-gui exists
  zCcliExists: boolean; //true, if file /usr/bin/zuluCrypt-cli exists
  unisonExists: boolean; //true, if file /usr/bin/unison-gtk exists
  PW: array[1..2] of string; // Password storage array
  prgstart: boolean;
  cmbii: integer;
  // [0] = Major version, [1] = Minor ver, [2] = Revision, [3] = Build Number
  // The above values can be found in the menu: Project > Project Options > Version Info
  verMajor, verMinor, verRev, verBuild: string;
  Info: TVersionInfo;

//convert Integer value to boolean:
function IntToBool(const IntVal: integer): boolean;
//Amount of Files in Directory Newer than given DateTime from last Sync
function FindNewer(var DirPath: string; var DateTimeString: string): integer;
//Returns State of Sync Status for recommended Color Value
function SyncStateColor(var LastSyncInfoExists, ContMounted,
  NewerInContExists, SyncContMounted, NewerInSyncContExists: boolean): shortint;


implementation

{$R *.lfm}

{ TfrmMain }

var
  PPh: string; // Random Passphrase for encryption
  SyncNrProcessing: integer; //Number of Sync (1-9) actually processing...
  SyncAllProcessing: boolean; //True, if Sync All Processing
  LastSyncTime: string; //time of last sync
  LastSyncAllTime: string; //time of last Overall Sync

// https://wiki.freepascal.org/Executing_External_Programs
procedure TProcessTimeout.LocalnIdleSleep(Sender, Context: TObject;
  status: TRunCommandEventCode; const message: string);
begin
  if status = RunCommandIdle then
  begin
    if (now - started) > timeoutperiod then
    begin
      timedout := True;
      Terminate(255);
      exit;
    end;
    sleep(RunCommandSleepTime);
  end;
end;
//https://wiki.freepascal.org/Executing_External_Programs
//https://forum.lazarus.freepascal.org/index.php?topic=3879.0
//https://stackoverflow.com/questions/20963310/lazarus-freepascal-reading-large-output-from-tprocess
function RunCommandTimeout(const exename: TProcessString;
  const commands: array of TProcessString; out outputstring: string;
  Options: TProcessOptions = []; SWOptions: TShowWindowOptions = swoNone;
  timeout: integer = 15): boolean;
var
  p: TProcessTimeout;
  i, exitstatus: integer;
  ErrorString: string;
begin
  p := TProcessTimeout.Create(nil);
  p.OnRunCommandEvent := p.LocalnIdleSleep;
  p.timeoutperiod := int64(timeout / SecsPerDay);
  if Options <> [] then
    P.Options := Options - [poRunSuspended, poWaitOnExit];
  p.options := p.options + [poRunIdle];
  //needed to run the RUNIDLE event. See User Changes 3.2.0

  P.ShowWindow := SwOptions;
  p.Executable := exename;
  if high(commands) >= 0 then
    for i := low(commands) to high(commands) do
      p.Parameters.add(commands[i]);
  p.timedout := False;
  p.started := now;
  try
    //the core loop of runcommand() variants, originally based on the "large output" scenario in the wiki, but continously expanded over 5 years.
    Result := p.RunCommandLoop(outputstring, errorstring, exitstatus) = 0;
    if p.timedout then
      Result := False;
  finally
    p.Free;
  end;
  if exitstatus <> 0 then
    Result := False;
end;

//Set Font at Runtime
//https://stackoverflow.com/questions/10588660/font-consistency-throughout-project
//Using: SetFontProperties(Self, 'Ubuntu Condensed', 10, []);
//Checking for Condensed Fonts on Linux via Terminal Command:
//fc-list |grep 'Condensed'
procedure TFrmMain.SetFontProperties(Control: TControl; FntName: TFontName;
  Size: integer; Styles: TFontStyles);
// Set font properties
var
  Index: integer;
  FntFont: TFont;
  AnObject: TObject;
  ChildControl: TControl;
begin
  // Set FntFont properties
  AnObject := GetObjectProp(Control, 'Font', nil);
  if AnObject is TFont then
  begin
    // Set properties
    FntFont := TFont(AnObject);
    FntFont.Name := FntName;
    FntFont.Size := Size;
    FntFont.Style := Styles;
  end;

  // Set child FntFont properties
  if Control is TWinControl then
  begin
    // Set
    for Index := 0 to TWinControl(Control).ControlCount - 1 do
    begin
      // Child control
      ChildControl := TWinControl(Control).Controls[Index];

      // Set FntFont properties
      SetFontProperties(ChildControl, FntName, Size, Styles);
    end;
  end;
end;

function TfrmMain.UnisonLogSummary: string; //returns the last log line from unison.log
var
  sl: TStringList;
  lastline: string;
begin
  lastline := '';
  if FileExists(GetUserDir + '/.unison/unison.log') then
  begin
    sl := TStringList.Create;
    sl.LoadFromFile(GetUserDir + '/.unison/unison.log');
    lastline := sl.Strings[sl.Count - 1];
    sl.Free;
  end
  else
    //The log file was deleted before synchronisation.
    //If no new logfile has been created, there is nothing to sync.
    lastline := 'Nothing to Synchronize';

  Result := lastline;
end;


//get Computer Name to show on top of UI
function GetMyComputerName: string;
  {$IfDef WINDOWS}
var
  l: DWORD;
  {$EndIf}
begin
      {$IfDef LINUX}
  Result := GetHostName;
      {$EndIf}
      {$IfDef WINDOWS}
  l := 255;
  SetLength(Result, l);
  GetComputerName(PChar(Result), l);
  SetLength(Result, l);
      {$EndIf}
      {$IfDef appleOS}
      {$EndIf}
end;

//Generate a Random UTF8 String for Password Encryption
function GenPwPhrase: string;
var
  s: string = '';
  i: integer;
begin
  for i := 1 to 32 do
    s := s + widechar(random($1FB00));
  Result := s;
end;

//Password Encryption with Rijndael encryption algorithm
//and randomly generated Passphrase to protect temporarly saved passwords for
//Encrypted Data Container (VeraCrypt).
//difference between Rijndael and AES:
//AES has a fixed block size of 128 bits and a key size of 128, 192, or 256 bits.
//Rijndael can be specified with block and key sizes in any multiple of 32 bits,
//with a minimum of 128 bits and a maximum of 256 bits.

// Encrypt Password String
function TfrmMain.EncryptPWStr(DecryptedPwStr: string): string;
var
  //https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
  Cipher: TDCP_rijndael;
  KeyStr: string;
begin
  KeyStr := PPh;
  Cipher := TDCP_rijndael.Create(Self);

  // initialize the cipher with a hash of the passphrase
  Cipher.InitStr(KeyStr, TDCP_sha256);

  // encrypt the contents of the Password Field
  Result := Cipher.EncryptString(DecryptedPwStr);
  Cipher.Burn;
  Cipher.Free;
end;

// Decrypt Password String
function TfrmMain.DecryptPWStr(EncryptedPwStr: string): string;
var
  //https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
  Cipher: TDCP_rijndael;
  KeyStr: string;
begin
  KeyStr := PPh;
  Cipher := TDCP_rijndael.Create(Self);

  // initialize the cipher with a hash of the passphrase
  Cipher.InitStr(KeyStr, TDCP_sha256);

  // encrypt the contents of the Password Field
  Result := Cipher.DecryptString(EncryptedPwStr);

  Cipher.Burn;
  Cipher.Free;
end;

// Cursor on Main Form while Waiting
procedure SetCursorWait;
var
  i: integer;
begin
  for i := 0 to frmMain.ControlCount - 1 do
    frmMain.Controls[i].Cursor := crHourGlass;
  frmMain.Cursor := crHourGlass;
end;

// Set Normal Cursor on Main Form
procedure SetCursorNormal;
var
  i: integer;
begin
  for i := 0 to frmMain.ControlCount - 1 do
    frmMain.Controls[i].Cursor := crDefault;
  frmMain.Cursor := crDefault;
end;


//convert Integer value to boolean:
//if IntVal = 0: Result := False
//if Intval > 0: Result := True
function IntToBool(const IntVal: integer): boolean;
begin
  if IntVal = 0 then
    Result := False
  else
    Result := True;
end;

function BoolToInt(const BoolVal: boolean): integer;
begin
  if BoolVal = True then
    Result := 1
  else
    Result := 0;
end;


procedure TfrmMain.FormCreate(Sender: TObject);
var
  pcname: string;
  i: integer;
  en_file: integer; //Semaphore value for given container x (1-9) file exists. (0/1)
  ensy_file: integer;
  //Semaphore value for given sync container x (11-19) file exists. (0/1)
  en_dir: integer;
  //Semaphore value for given container x (1-9) directory (=mounted) exists. (0/1)
  ensy_dir: integer;
  //Semaphore value for given sync container x (11-19) directory (=mounted) exists. (0/1)
  //Semaphore value for given sync container x (11-19) directory (=mounted) exists. (0/1)
  lockextflag: boolean; // check if sync container min. once among the drives
  rs: ansistring;  //return String for hostnamectl info
  fontexists: boolean;
  S: TResourceStream; //Resource Content Handling
  F: TFileStream;  //Resource Content Handling
  Process: TProcess; //for font cache refresh command (cmd)
  aProcess: TProcess; //restarting self

begin
  //Check if Ubuntu Condensed Font exists
  fontexists := FileExists(GetUserDir + '/.fonts/ucondensed.ttf') or
    FileExists('/usr/share/fonts/truetype/ubuntu/ucondensed.ttf');

  if fontexists then

    if not fontexists then
    begin
      ShowMessage('Font /usr/share/fonts/truetype/ubuntu/ucondensed.ttf does not exist');
      //     LOAD CUSTOM FONT from RESOURCE
      //Create local Font Dir for Ubuntu Condensed Font, if not exists
      if not DirectoryExists(GetUserDir + '/.fonts') then
        if not CreateDir(GetUserDir + '/.fonts') then
          ShowMessage('Failed to create local font directory!');

      //Using resources:
      //https://wiki.freepascal.org/Lazarus_Resources#Linux
      //https://wiki.freepascal.org/IDE_Window:_Project_Options#Resources
      //https://wiki.freepascal.org/fcl-res
      //extract the stored resource (font) into the file ucondensed.ttf:
      // create a resource stream which points to our resource
      S := TResourceStream.Create(HInstance, 'UCONDENSED', RT_RCDATA);
      // Please ensure you write the enclosing apostrophes around DATA,
      // otherwise no data will be extracted.
      try
        // create a file ucondensed.ttf in the application directory
        //F := TFileStream.Create(ExtractFilePath(ParamStr(0)) + 'Ubuntu-C.ttf', fmCreate);
        F := TFileStream.Create(GetUserDir + '/.fonts/ucondensed.ttf', fmCreate);
        try
          F.CopyFrom(S, S.Size); // copy data from the resource stream to file stream
        finally
          F.Free; // destroy the file stream
        end;
      finally
        S.Free; // destroy the resource stream
      end;

      //Refresh Font Cache (for Ubuntu Condensed)
      Process := TProcess.Create(nil);
      //$ fc-cache -fv
      try
        Process.CommandLine := 'fc-cache -fr';
        Process.Options := Process.Options + [poWaitOnExit];
        Process.Execute;
        Process.Free;
      except
        Process.Free;
        ShowMessage('Error while refreshing Font Cache at Start');
      end; //try

    {
     //Setting Font (Ubuntu Condensed from Resource File) Recursively for all components
     // Not Working on Linux, only in Windows: Application.DefaultFont := 'Ubuntu Condensed';
     try
       SetFontProperties(Self, 'Ubuntu Condensed', 12, []);
       //SetFontProperties(Self, 'Nimbus Sans Narrow Regular', 10, []);
     except
       ShowMessage('Font [Ubuntu Condensed] does not exists');
     end;
    }

      //after copying new font, restart Application to apply changes
      Delay(2000);
      aProcess := TProcess.Create(nil);
      aProcess.CommandLine := '"' + Application.ExeName + '"';
      aProcess.Execute;
      aProcess.Free;
      Application.Terminate;
    end;
  {
   else
     SetFontProperties(Self, 'Ubuntu Condensed', 12, []);
  }

  //     END LOAD CUSTOM FONT


  Info := TVersionInfo.Create;
  // [0] = Major version, [1] = Minor ver, [2] = Revision, [3] = Build Number
  // The above values can be found in the menu: Project > Project Options > Version Info
  Info.Load(HINSTANCE);
  // grab the entire Version Number
  // verMajor, verMinor, verRev, verBuild : String;
  verMajor := IntToStr(Info.FixedInfo.FileVersion[0]);
  verMinor := IntToStr(Info.FixedInfo.FileVersion[1]);
  verRev := IntToStr(Info.FixedInfo.FileVersion[2]);
  verBuild := IntToStr(Info.FixedInfo.FileVersion[3]);
  Info.Free;
  // Update the title string - include the version & ver #
  //frmMain.Caption := 'zcryptui ' + verMajor + '.' + verMinor + '.' +
  //  verRev + '.' + verBuild;
  //frmMain.Caption := 'zcryptui ' + verMajor + '.' + verMinor + '.' + verRev;

  prgstart := True;

  frmMain.Height := btnUmountSyncDrives.Top + btnUmountSyncDrives.Height + 4;
  uiNormalWidth := btnC9eject.Left + btnC9eject.Width + 4;
  uiExtWidth := btnUmountSyncDrives.Left + btnUmountSyncDrives.Width + 4;
  frmMain.Width := uiNormalWidth;

  XMLPropStorage1.Restore;

  XMLPropStorage1.FileName := ExtractFilePath(ParamStr(0)) + '.' +
    ExtractFileName(ParamStr(0));

  uiSmallOnce := False;
  self.BorderIcons := [biSystemMenu]; //enable only close button on Form Border

  PPh := GenPWPhrase; // Generate new random PassPhrase for Password Encryption
  unisonExists := FileExists('/usr/bin/unison-gtk');
  //btnSyncC1.Enabled := unisonExists;
  zCguiExists := FileExists('/usr/bin/zuluCrypt-gui');
  //btnZCgui.Enabled := zCguiExists;
  zCcliExists := FileExists('/usr/bin/zuluCrypt-cli');
  if not zCcliExists then
  begin
    if MessageDlg('ContainerGui: Missing zuluCrypt-cli',
      'Program zuluCrypt-cli not found. This Application needs zuluCrypt-cli to work. Download and install it first.',
      mtWarning, [mbYes], 0) = mrYes then
      Application.Terminate;
    Halt(1);
  end;
  try
    pcname := GetMyComputerName;
  except
    on e: Exception do
      Writeln(e.Message);
  end;

  pnlHostInfo.Caption := GetEnvironmentVariable('USER') + '@' + pcname;

  //get running environment info with hostnamectl in bash
  //https://www.howtogeek.com/803839/how-to-let-linux-scripts-detect-theyre-running-in-virtual-machines/
  RunCommand('/bin/bash', ['-c', 'hostnamectl|grep Chassis'], rs);
  if (Pos('Chassis: vm', rs)) > 0 then
    OutSide := True
  else
    OutSide := False;

  case OutSide of
    True: //Paths and Files Outside of Home
      pnlHostInfo.Color := clYellow;
    False: //Paths and Files at Home
      pnlHostInfo.Color := clLime;
  end;

  lockextflag := False;

  for i := 1 to 19 do
  begin
    if i <> 10 then
    begin

      // processing enabling nonSYNC containers
      en_file := BoolToInt(FileExists(Containers[i].FileName));
      en_dir := BoolToInt(DirectoryExists(Containers[i].Directory));

      (FindComponent('btnOpenC' + IntToStr(i)) as TBitBtn).Enabled := IntToBool(en_file);

      (FindComponent('btnOpenC' + IntToStr(i)) as TBitBtn).ImageIndex := en_dir;
      (FindComponent('btnC' + IntToStr(i) + 'eject') as TSpeedButton).Enabled :=
        IntToBool(en_dir);

      // processing enabling SYNC containers
      if i > 10 then
      begin
        ensy_file := BoolToInt(FileExists(Containers[i].FileName));
        ensy_dir := BoolToInt(DirectoryExists(Containers[i].Directory));

        // check if external/sync container
        // min. once among the drives
        if not lockextflag then
          if IntToBool(ensy_file) = True then
          begin
            lockextflag := True;
            syncdriveispresent := True;
            frmMain.Width := uiExtWidth;
            btnExtendUi.Enabled := False;
            uiSmallOnce := False;
          end
          else
          begin
            btnExtendUi.Enabled := True;
            if not uiSmallOnce then
              // if ext/sync media removed, collapse once the ui to normal width.
              // The uiSmallOnce Flag will be reset on raising of lockextflag at the next time
            begin
              uiSmallOnce := True;
              frmMain.Width := uiNormalWidth;
            end;
          end;
      end;
    end;
  end;

  frmMain.ShowInTaskBar := stNever;
  frmMain.Visible := False;
  frmMain.Hide;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  btnFlushClick(Self);
end;

procedure TfrmMain.FormHide(Sender: TObject);
begin
  pop_ShowHide.Caption := 'Show';
  XMLPropStorage1.Save; //need for save position of main form
  if frmConfig.chkFlushOnMinimize.Checked = True then
    btnFlush.Click;
end;

procedure TfrmMain.FormPaint(Sender: TObject);
begin
  if prgstart = True then
  begin
    if pop_startMinimized.Checked = True then
      frmMain.Hide;
    prgstart := False;
  end;
  btnFlush.Enabled := IntToBool(Length(PW[1] + PW[2]));
end;

procedure TfrmMain.FormResize(Sender: TObject);
begin
  //detecting Form move/resize
  frmMain.Height := btnUmountSyncDrives.Top + btnUmountSyncDrives.Height + 4;
  if frmMain.Width > uiExtWidth then
    frmMain.Width := uiExtWidth;
  XMLPropStorage1.Save; //need for saving position of main form
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  PopupMenu1.Close;
  pop_ShowHide.Caption := 'Hide';
  if pop_alwaysOnTop.Checked = True then
    frmMain.FormStyle := fsSystemStayOnTop
  else
    frmMain.FormStyle := fsNormal;

  //restart Inactivity Timer anew if Showing Form
  tInactivity.Enabled := False;
  tInactivity.Enabled := True;
end;

procedure TfrmMain.PopupMenu1Popup(Sender: TObject);
var
  pos: TPoint;
begin
  pos := Mouse.CursorPos;
  //necessary for multi monitor setup, simle popup does not work correctly!
  PopupMenu1.Popup(pos.X, pos.Y - 120);
  pop_ShowHideClick(PopupMenu1);
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  if frmMain.Visible then
    //Form can close only from PopupMenu in tray panel...
    CanClose := False
  else
    CanClose := True;
  //...therefore, hide it.
  frmMain.Hide;
end;


//Amount of Files in Directory Newer than given DateTime from last Sync
function FindNewer(var DirPath: string; var DateTimeString: string): integer;
var
  Process: TProcess;
  AStringList: TStringList;

begin
  //Deactivate Function FindNewer (causes Error (prg hangs) in Versions 1.1.0.8-1.1.0.9 (2022-12-09))
  //bug: Probable error with config handling. Solution: Deleting old Config file .zcryptui and create a new one at next start
  //Result := 0;
  //exit;

  if (Length(DirPath) > 0) and (Length(DateTimeString) > 0) then
  begin
    Process := TProcess.Create(nil);
    // Erzeugen des TStringList Objekts.
    AStringList := TStringList.Create;

    //Bash: find /run/media/private/lxmm/WOFX -type f -newermt "2022-11-04 07:27:30"
    try
      //Process.Executable := 'find';
      Process.CommandLine := 'find ' + DirPath + ' -type f -newermt ' +
        '"' + DateTimeString + '"';
      //ShowMessage(Process.CommandLine);
      Process.Options := Process.Options + [poUsePipes];
      Process.Execute;

      while Process.ExitStatus < 0 do
      begin //use this instead of process.Option poWaitOnExit
        //wait until last process ended
        Application.ProcessMessages;
      end;

      // The following will only be executed after process.executable has finished:
      // The output is now read into the string list
      AStringList.LoadFromStream(Process.Output);

      //the Result is the Number of Lines (Amount of newer Files) in Stringlist
      Result := AStringList.Count;

      // Saves the output to a file.
      //AStringList.SaveToFile('output.txt');


      Process.Free;
      AStringList.Free;
    except
      Process.Free;
      AStringList.Free;
      //ShowMessage('File Date checking Process: Exception occured');
      Result := 0;
    end; //try
  end  //if (Length(DirPath) > 0) and (Length(DateTimeString) > 0)
  else //if (Length(DirPath) > 0) and (Length(DateTimeString) > 0)
    Result := 0;

end;  //function FindNewer(var DirPath: string; var DateTime: string): Integer;


//Returns State of Sync Status for recommended Color Value
function SyncStateColor(var LastSyncInfoExists, ContMounted,
  NewerInContExists, SyncContMounted, NewerInSyncContExists: boolean): shortint;
  //Return Values:
  //0: GREEN, No Sync Required
  //1: YELLOW, Sync Maybe Required
  //2: RED, Sync Required
  //3: GREY, Sync Determination Not Possible
begin
  if not ContMounted and not SyncContMounted then
    Result := 3
  else
  if (ContMounted and NewerInContExists) or (SyncContMounted and
    NewerInSyncContExists) then
    Result := 2
  else
  if not LastSyncInfoExists or not ContMounted or not SyncContMounted then
    Result := 1
  else
  if LastSyncInfoExists and ContMounted and SyncContMounted and not
    NewerInContExists and not NewerInSyncContExists then
    Result := 0;
  {
   if not ContMounted and not SyncContMounted then
     Result := 3
   else
   if LastSyncInfoExists and (not ContMounted or not SyncContMounted) and
     (not NewerInContExists or not NewerInSyncContExists) then
     Result := 1
   else
   if (ContMounted and NewerInContExists) or (SyncContMounted and
     NewerInSyncContExists) then
     Result := 2
   else
   if LastSyncInfoExists and ContMounted and SyncContMounted and not
     NewerInContExists and not NewerInSyncContExists then
     Result := 0;
  }

end; //function SyncStateColor


procedure TfrmMain.tPeriodicIdleTimerTimer(Sender: TObject);
var
  i: integer;
  en_file: integer; //Semaphore value for given container x (1-9) file exists. (0/1)
  ensy_file: integer;
  //Semaphore value for given sync container x (11-19) file exists. (0/1)
  en_dir: integer;
  //Semaphore value for given container x (1-9) directory (=mounted) exists. (0/1)
  ensy_dir: integer;
  //Semaphore value for given sync container x (11-19) directory (=mounted) exists. (0/1)
  MountedSyncContainersCount: integer;
  //mounted containers counter on external drive for sync
  MountedContainersCount: integer; //mounted containers (non sync containers)
  lockextflag: boolean; // check if sync container min. once among the drives
  LastSyncInfo: boolean; //True, if LastSyncTime Exists
  HintTextSyncBtn: string; //hint text of sync buttons
  HintLengthSyncBtn: integer; //hint length on sync buttons
  HintTextContBtn: string; //hint text of container buttons
  HintTextSyncContBtn: string; //hint text of Sync container buttons

  syncneedgeneral: integer; //Semaphore if Sync need in general for one of the containers
  //statestringlist: TStringList; //string holder for state Strings

  ContainerFileExists: boolean; //Container File Exists (encrypted container File)
  SyncContainerFileExists: boolean;
  //Sync-Container File Exists (encrypted sync-container File)
  SyncCfgFileExists: boolean; //Unison Sync Configuration Exists

  ContainerMounted: boolean; //Regular Container (Path on Left Side) exists
  SyncContainerMounted: boolean; //SYNC Container (Path on Right Side) exists

  NewerInSyncContCnt: integer; //Amount of Newer files found in SYNC Container
  NewerInContCnt: integer; //Amount of Newer Files found in Regular container
  NewerInSyncContExists: boolean;
  NewerInContExists: boolean;

  SyncPairsPresent: boolean; //Container Pair Mounted and cfg exists (Ready for Sync)

  //Semaphore for one of following states in MicroStatus-Display exists.
  //need for checking overall State Requirement for Conatainers.
  GREENexists: boolean;
  YELLOWexists: boolean;
  REDexists: boolean;
  GREYexists: boolean;
  MediaPresent: boolean; //check if Sync Media present

begin
  MediaPresent := FileExists(fileonmedia);

  if MediaPresent then
  begin
    //show frm Caption Alert for plugged sync device
    if frmMain.Caption = 'zCryptUI' then
      frmMain.Caption := '[ !!! Handle Drive !!! ]'
    else
      frmMain.Caption := 'zCryptUI';
  end
  else
    frmMain.Caption := 'zCryptUI';

  //Do nothing for UI if not showing
  if not frmMain.Showing then
    //Show Form always if sync Drive Path is Present
    if MediaPresent then frmMain.Show
    else
      exit;

  lockextflag := False;
  MountedSyncContainersCount := 0;
  MountedContainersCount := 0;
  SyncPairsPresent := False;
  SyncPairsCount := 0;
  syncneedgeneral := 0;

  GREENexists := False;
  YELLOWexists := False;
  REDexists := False;
  GREYexists := False;

  for i := 1 to 9 do //Handle All Components (1-9, 11-19)
  begin

    //prepare detection of mounted Folders
    en_file := BoolToInt(FileExists(Containers[i].FileName));
    en_dir := BoolToInt(DirectoryExists(Containers[i].Directory));

    ensy_file := BoolToInt(FileExists(Containers[i + 10].FileName));
    ensy_dir := BoolToInt(DirectoryExists(Containers[i + 10].Directory));

    ContainerFileExists := FileExists(Containers[i].FileName);
    ContainerMounted := DirectoryExists(Containers[i].Directory);

    SyncContainerFileExists := FileExists(Containers[i + 10].FileName);
    SyncContainerMounted := DirectoryExists(Containers[i + 10].Directory);

    SyncCfgFileExists := FileExists(Containers[i].SyncProfile);

    //Enabling container handling buttons if encrypted container file exists
    (FindComponent('btnOpenC' + IntToStr(i)) as TBitBtn).Enabled :=
      IntToBool(en_file);
    (FindComponent('btnOpenC' + IntToStr(i + 10)) as TBitBtn).Enabled :=
      IntToBool(ensy_file);

    //Set mounted/unmounted folder images on container handling buttons
    (FindComponent('btnOpenC' + IntToStr(i)) as TBitBtn).ImageIndex := en_dir;
    (FindComponent('btnOpenC' + IntToStr(i + 10)) as TBitBtn).ImageIndex := ensy_dir;

    //Enabling eject buttons if encrypted container folders are present
    (FindComponent('btnC' + IntToStr(i) + 'eject') as TSpeedButton).Enabled :=
      IntToBool(en_dir);
    (FindComponent('btnC' + IntToStr(i + 10) + 'eject') as TSpeedButton).Enabled :=
      IntToBool(ensy_dir);

    SyncPairsPresent := ContainerMounted and SyncContainerMounted and SyncCfgFileExists;

    //Enable Sync Buttons if Container, Sync-Container and
    //the corresponding Sync-Profile exists
    (FindComponent('btnSyncC' + IntToStr(i)) as TSpeedButton).Enabled :=
      SyncPairsPresent;

    if SyncPairsPresent then
      Inc(SyncPairsCount);

    //Count Mounted Regular Containers (left side)
    MountedContainersCount :=
      MountedContainersCount + BoolToInt(ContainerMounted);

    //Count Mounted Sync Containers (right side)
    MountedSyncContainersCount :=
      MountedSyncContainersCount + BoolToInt(SyncContainerMounted);

    //check if sync container min. once among the drives
    //if true, trigger extending ui once to double width
    if not lockextflag then
      if IntToBool(ensy_file) = True then
      begin
        lockextflag := True;
        syncdriveispresent := True;
        frmMain.Width := uiExtWidth;
        btnExtendUi.Enabled := False;
        uiSmallOnce := False;
      end
      else
      begin
        btnExtendUi.Enabled := True;
        if not uiSmallOnce then
          // if ext/sync media removed, collapse once the ui to normal width.
          // The uiSmallOnce Flag will be reset on raising of lockextflag at the next time
        begin
          uiSmallOnce := True;
          frmMain.Width := uiNormalWidth;
          syncdriveispresent := False;
        end;
      end;

    //Enable Unmount ALL Mounted Regular Containers
    btnUmountAll.Enabled := IntToBool(MountedContainersCount);

    //Enable Unmount ALL Mounted SYNC Containers
    //only if Sync Drive is present and no encrypted containers are mounted
    btnUmountSyncDrives.Enabled :=
      syncdriveispresent and (IntToBool(MountedSyncContainersCount));

    //get last sync time (stored in sync-button hint) for container
    //Format: LastSyncTime := '2022-11-05 06:13:30';
    HintTextSyncBtn := (FindComponent('btnSyncC' + IntToStr(i)) as TSpeedButton).Hint;
    HintLengthSyncBtn := Length(HintTextSyncBtn);
    //get last sync time, if hint longer than 20 characters
    if HintLengthSyncBtn > 20 then
      LastSyncTime := Copy(HintTextSyncBtn, HintLengthSyncBtn - 19, 19)
    else
      LastSyncTime := '';

    //Last Sync Information Exists?
    LastSyncInfo := IntToBool(Length(LastSyncTime));

    //assign Hint on MouseOver to Mounted Folder with Newest FileDate info (Regular Container)
    if ContainerMounted then
    begin //handle sync time state for Container x (of 1-9) if mounted
      HintTextContBtn := 'Open Directory';
      //generate more recent File Date in Regular Container
      NewerInContCnt := FindNewer(Containers[i].Directory, LastSyncTime);
      NewerInContExists := IntToBool(NewerInContCnt);
      //Assign Number of Newer Files in Button Indicator
      (FindComponent('txtNewer' + IntToStr(i)) as TStaticText).Caption :=
        IntToStr(NewerInContCnt);

      if NewerInContCnt > 0 then
      begin
        (FindComponent('txtNewer' + IntToStr(i)) as TStaticText).Enabled :=
          True;
        (FindComponent('txtNewer' + IntToStr(i)) as TStaticText).Font.Color :=
          clRed;
        HintTextContBtn := HintTextContBtn + #13 + 'Newer Files: ' +
          IntToStr(NewerInContCnt);
        Inc(syncneedgeneral);
      end
      else
      begin
        (FindComponent('txtNewer' + IntToStr(i)) as TStaticText).Enabled :=
          False;
        (FindComponent('txtNewer' + IntToStr(i)) as TStaticText).Font.Color :=
          clDefault;
        HintTextContBtn := HintTextContBtn + #13 + 'No Newer Files since last Sync';
      end;
    end; //if Mounted Regular Container exists

    //assign Hint on MouseOver to Mounted Folder with Newest FileDate info (Sync Container)
    if SyncContainerMounted then
    begin //handle sync time state for Sync Container x (of 11-19) if mounted
      HintTextSyncContBtn := 'Open Directory';
      //get amount of more recent Files in Sync Container
      NewerInSyncContCnt := FindNewer(Containers[i + 10].Directory, LastSyncTime);
      NewerInSyncContExists := IntToBool(NewerInSyncContCnt);
      //Assign Number of Newer Files in Button Indicator
      (FindComponent('txtNewer' + IntToStr(i + 10)) as TStaticText).Caption :=
        IntToStr(NewerInSyncContCnt);

      if NewerInSyncContCnt > 0 then
      begin
        (FindComponent('txtNewer' + IntToStr(i + 10)) as TStaticText).Enabled :=
          True;
        (FindComponent('txtNewer' + IntToStr(i + 10)) as TStaticText).Font.Color :=
          clRed;
        HintTextSyncContBtn :=
          HintTextSyncContBtn + #13 + 'Newer Files: ' + IntToStr(NewerInSyncContCnt);
        Inc(syncneedgeneral);
      end
      else
      begin
        (FindComponent('txtNewer' + IntToStr(i + 10)) as TStaticText).Enabled :=
          False;
        (FindComponent('txtNewer' + IntToStr(i + 10)) as TStaticText).Font.Color :=
          clDefault;
        HintTextSyncContBtn :=
          HintTextSyncContBtn + #13 + 'No Newer Files since last Sync';
      end;

    end;//if Mounted Sync Container exists


    if ContainerFileExists and not ContainerMounted then
      HintTextContBtn := 'Mount Encrypted Container';
    if SyncContainerFileExists and not SyncContainerMounted then
      HintTextSyncContBtn := 'Mount Encrypted Container';

    //Assign Hint Text to Button
    (FindComponent('btnOpenC' + IntToStr(i)) as TBitBtn).Hint :=
      HintTextContBtn;
    (FindComponent('btnOpenC' + IntToStr(i + 10)) as TBitBtn).Hint :=
      HintTextSyncContBtn;


    //Return Values:
    //0: GREEN, No Sync Required
    //1: YELLOW, Sync Maybe Required
    //2: RED, Sync Required
    //3: GREY, Sync Determination Not Possible
    case SyncStateColor(LastSyncInfo, ContainerMounted, NewerInContExists,
        SyncContainerMounted, NewerInSyncContExists)
      of
      // $0040FF40 for GREEN color, no newer file than sync time exists
      0:
      begin
        TPanel(FindComponent('pnlSyncState' + IntToStr(i))).Color := $0040FF40;
        GREENexists := True;
      end;
      // $008BFDFD for YELLOW color (NO Timestamp Exists)
      1:
      begin
        TPanel(FindComponent('pnlSyncState' + IntToStr(i))).Color := $008BFDFD;
        YELLOWexists := True;
      end;
      // $004040FF for RED color, newer file than sync time exists
      2:
      begin
        TPanel(FindComponent('pnlSyncState' + IntToStr(i))).Color := $004040FF;
        REDexists := True;
      end;
      // $00D9D9D9 for GREY color (unknown state)
      3:
      begin
        TPanel(FindComponent('pnlSyncState' + IntToStr(i))).Color := $00D9D9D9;
        GREYexists := True;
      end;
    end;//case SyncStateColor;

  end;//for i := 1 to 9 do //Handle All Components (1-9, 11-19)

  //Enable Sync ALL Button, if at least one of Sync Pairs exists
  //or if AutoMount on Sync configured, if one of regular Containers mounted

  if frmConfig.chkMntSyncAuto.Checked then
    btnSyncAll.Enabled := IntToBool(MountedContainersCount)
  else
    btnSyncAll.Enabled := IntToBool(SyncPairsCount);

  if not IntToBool(SyncNrProcessing) and not SyncAllProcessing then
  begin //apply overall container state color and Text only if not Syncing
    if REDexists then
    begin
      pnlSyncInfo.Color := $004040FF;
      pnlSyncInfo.Caption := 'Sync Required!';
      frmMain.Width := uiExtWidth;
    end
    else
    if YELLOWexists then
    begin
      pnlSyncInfo.Color := $008BFDFD;
      //pnlSyncInfo.Caption := 'Sync may be required';
      //pnlSyncInfo.Caption := 'Sync:' + LastSyncAllTime;
      //...pnlSyncInfo.Caption will be set after every Sync
    end
    else
    if GREENexists then
    begin
      pnlSyncInfo.Color := $0040FF40;
      //pnlSyncInfo.Caption := 'No Sync Required';
    end
    else
    if GREYexists then
    begin
      pnlSyncInfo.Color := $00D9D9D9;
      pnlSyncInfo.Caption := 'No Mounted Containers';
    end;

  end;
    {
    if IntToBool(syncneedgeneral) then
    begin
      pnlSyncInfo.Caption := 'Sync Required';
      pnlSyncInfo.Color := $004040FF;
    end
    else
    begin
      pnlSyncInfo.Caption := 'No Sync Required';
      pnlSyncInfo.Color := clLime;
    end;
    }

end;  //IdleTimer1Timer


procedure TfrmMain.tSyncDoneTimer(Sender: TObject);
var
  i: integer;
begin
  if syncDone then
  begin
    // set SyncButton Images to sync-icon
    for i := 1 to 9 do
    begin
      (FindComponent('btnSyncC' + IntToStr(i)) as TSpeedButton).ImageIndex := 3;
    end;

    tBlink.Enabled := False;
    //pnlSyncInfo.Color := clDefault;
    btnSyncAll.ImageIndex := 8;
    syncDone := False;
  end; //if syncDone

  tSyncDone.Enabled := False;
end;


//writes any data buffered in memory out to disk
//if Result = 0, everything is ok.
//help with examples: https://www.computerhope.com/unix/sync.htm
function tfrmMain.OSSyncStorage(DirPath: string): integer;
  //Sync Storage Writing via Terminal Command
var
  Process: TProcess;
  InfoText: string;

begin
  InfoText := pnlSyncInfo.Caption;
  pnlSyncInfo.Caption := 'Write Data...';
  Process := TProcess.Create(nil);
  try
    Process.CommandLine := 'sync ' + DirPath;
    Process.Options := Process.Options + [poUsePipes];
    Process.Execute;
    //wait until OS-Sync Finished and process messages in Application
    while Process.ExitStatus < 0 do
    begin //use this instead of process.Option poWaitOnExit
      //wait until last process ended
      Application.ProcessMessages;
    end;

    Result := Process.ExitStatus;
    Process.Free;
    pnlSyncInfo.Caption := InfoText;

  except
    Process.Free;
    ShowMessage('Storage Sync Process Exception occured');
  end;
end;

//Synchronize Encrypted Containers
procedure TfrmMain.btnSyncContainerClick(Sender: TObject);
var
  outp: string;
  btnid: integer; //caller id (Sync-Speedbutton Tag)
  syncprofile: string; //sync profile name
  logfile: string; //log file name for unison (cmd)
  formattedparams: string; //sync profile name, formatted for command string

  procUnison: TProcess; //external Process for Unison
  //procSleep: TProcess; //external Process for Sleep
  AStringList: TStringList;
  out: integer; //out index in stringlist.find
  pstring: string;
  cnt: integer;
  cntres: integer;
  exitstatus: integer;
  SyncT: string; //Sync Time for Sync Info Panel on the Top
  LastTimeStamp: string; //last generated timestamp
  ExtraSyncMsg: string; //log msg for extra sync

begin
  frmMain.Show;

  //first, generally backup unison log file and delete last unison log
  //if nothing sinchronized, unison will not creating any log file
  if FileExists(GetUserDir + '/.unison/unison.log') then
  begin
    if CopyFile(GetUserDir + '/.unison/unison.log', GetUserDir +
      '/.unison/unison.log.old', [cffOverwriteFile]) then
      DeleteFile(GetUserDir + '/.unison/unison.log');
  end;

  SetCursorWait;
  //get caller Sync-Speedbutton Tag
  btnid := (Sender as TSpeedButton).Tag - 20;
  SyncNrProcessing := btnid;

  //Run Extra Sync if Activated in Config and Sync All not Running
  //Extra Sync will executing in Sync-All Routine
  if frmConfig.chkExtraSyncOn.Checked then
  begin
    if not SyncAllProcessing then
    begin
      frmConfig.btnExtraSyncClick(Self);
      ExtraSyncMsg := 'Extra Sync EXECUTED';
      LastTimeStamp := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
    end;
  end
  else //if not frmConfig.chkExtraSyncOn.Checked
  begin
    if not SyncAllProcessing then
    begin
      ExtraSyncMsg := 'Extra Sync NOT ACTIVATED';
      LastTimeStamp := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
    end;
  end;

  if not SyncAllProcessing then
  begin
    //Save Extra Sync Log Info
    Containers[10].LastSyncTimeStamp := LastTimeStamp;
    Containers[10].LastSyncMessage := ExtraSyncMsg;
    frmSyncLog.grdSyncLog.Cells[1, 10] := Containers[10].LastSyncTimeStamp;
    frmSyncLog.grdSyncLog.Cells[2, 10] := Containers[10].LastSyncMessage;
  end;

  (Sender as TSpeedButton).ImageIndex := 3;
  pnlSyncInfo.Color := $0000C2FF;
  tBlink.Enabled := True;


  //generate sync profile name from btn caller id and cfg-file
  syncprofile := ExtractFileNameOnly(Containers[btnid].SyncProfile);
  //generate logfile name
  //-logfile "01_test-unis1-unis2-$(date +"%y%m%d%H%M%S").log"
  //logfile := syncprofile + '.log';

  //format sync profile name and params for RunCommand
  formattedparams := ' ''' + syncprofile + '''';

  procUnison := TProcess.Create(nil);
  //procSleep := TProcess.Create(nil);
  try
    //use sleep command for visual reasons only, if sync finished too fast
    //procSleep.Executable := 'sleep';
    //procSleep.Options := [poWaitOnExit]; //if use, application 'hangs'
    //procSleep.Parameters.Add('3');
    //procSleep.Execute;
    //while procSleep.ExitStatus < 0 do
    //begin //use this instead of process.Option poWaitOnExit
    //wait until last process ended
    //Application.ProcessMessages;
    //end;

    //procUnison.ShowWindow := swoShowDefault; //Windows only
    procUnison.Executable := 'unison';
    procUnison.Parameters.Add(syncprofile);
    //do not add -logfile parameter, this causes error while syncing
    //if profile contains zcryptui working dir
    //procUnison.Parameters.Add('-logfile');
    //procUnison.Parameters.Add(logfile);

    //procUnison.Options := procUnison.Options + [poWaitOnExit, poUsePipes];
    //bug: TProcess with [poWaitOnExit,poUsePipes] options hangs
    //https://forum.lazarus.freepascal.org/index.php?topic=5982.0

    //procUnison.Options := procUnison.Options + [poWaitOnExit]; //if use, application 'hangs'
    procUnison.Execute;
    while procUnison.ExitStatus < 0 do
    begin //use this instead of process.Option poWaitOnExit
      //wait until last process ended
      Application.ProcessMessages;
    end;

    //AStringList := TStringList.Create;
    //AStringList.LoadFromStream(procUnison.Stderr);
    //Memo1.Lines.Append(AStringList.Text);
    //AStringList.SaveToFile('output.txt');

    //Memo1.Lines.Append('Process exit code: ' + IntToStr(procUnison.ExitCode));
    //unison exit status:
    //0 = everything is ok
    //1 = something fails
    exitstatus := procUnison.ExitStatus;
    //Memo1.Lines.Append('unison exit status: ' + IntToStr(exitstatus));

    procUnison.Free;
    //procSleep.Free;

    (Sender as TSpeedButton).Hint :=
      'Sync with Unison' + #13 + '(last Sync: ' +
      FormatDateTime('yyyy-mm-dd hh:nn:ss', Now) + ')';

    {
     if OSSyncStorage = 0 then
       (Sender as TSpeedButton).Hint :=
         'Sync with Unison' + #13 + '(last Sync: ' +
         FormatDateTime('yyyy-mm-dd hh:nn:ss', Now) + ')';
    }

    //check if unison procUnison exited with status code gt 0
    //means: if anything while sync is skipped or failed
    if exitstatus > 0 then
      //if AStringList.Text.Contains('skipped') then
    begin
      //The replicas are not fully synchronized. There was skipped item(s).
      //Memo1.Lines.Append('Unison has skipped items in replicas. Unison-gtk will be started...');
      ShowMessage('Unison has skipped items in replicas, Unison-gtk will be started...');
      Application.ProcessMessages;
      // Unison-GTK Variant will be started
      if not AppIsRunning('unison-gtk') then
        if RunCommand('unison-gtk' + formattedparams, outp) then
        begin
          (Sender as TSpeedButton).Hint :=
            'Sync with Unison' + #13 + '(last Sync: ' +
            FormatDateTime('yyyy-mm-dd hh:nn:ss', Now) + ')';
        end
        else
        begin
          if MessageDlg('zcryptui: Unison Error',
            'Unison Error occured. Check if the Unison software is installed correctly.',
            mtWarning, [mbYes], 0) = mrYes then
          ;

        end;
      //Memo1.Lines.Append('unison-gtk is finished.');
      (Sender as TSpeedButton).ImageIndex := 11;
      syncDone := True;
      tSyncDone.Enabled := False;
    end
    else
    begin
      //Memo1.Lines.Append('unison background task is finished correctly.');
      (Sender as TSpeedButton).ImageIndex := 11;
      syncDone := True;
      tSyncDone.Enabled := False;
    end;

  except
    procUnison.Free;
    //Memo1.Lines.Append('Process Exception occured: ?');
    ShowMessage('Unison Background Process Exception occured');
    AStringList.Free;
  end;
  //Memo1.Lines.Append('Process Finished.');
  //Memo1.Lines.Append('-----------------');

  //run OS-Sync command in terminal to write all data to data storage...
  //Regular Container OS-Sync Function
  pnlSyncInfo.Caption := 'Write Data...';
  {
   if (DirectoryExists(Containers[(Sender as TSpeedButton).Tag - 20].Directory)) then
     if OSSyncStorage(Containers[(Sender as TSpeedButton).Tag - 20].Directory) = 0 then;
  }
  //Application.ProcessMessages;
  //Sync Container OS-Sync Function
  //pnlSyncInfo.Caption := 'Write Data...';
  {
   if (DirectoryExists(Containers[(Sender as TSpeedButton).Tag - 10].Directory)) then
     if OSSyncStorage(Containers[(Sender as TSpeedButton).Tag - 10].Directory) = 0 then;
  }
  //Application.ProcessMessages;

  SetCursorNormal;
  SyncNrProcessing := 0;
  tBlink.Enabled := False;
  //pnlSyncInfo.Color := clDefault;
  (FindComponent('btnSyncC' + IntToStr(btnid)) as TSpeedButton).ImageIndex := 11;
  Application.ProcessMessages;
  SyncT := FormatDateTime('yy-mm-dd hh:nn', Now);
  pnlSyncInfo.Caption := 'Synced:' + SyncT;

  LastTimeStamp := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);

  //Save Container specific Log Info
  Containers[btnid].LastSyncTimeStamp := LastTimeStamp;
  //if UnisonLogSummary <> '' then
  Containers[btnid].LastSyncMessage := UnisonLogSummary;

  frmSyncLog.grdSyncLog.Cells[1, btnid] := Containers[btnid].LastSyncTimeStamp;
  frmSyncLog.grdSyncLog.Cells[2, btnid] := Containers[btnid].LastSyncMessage;

  frmSyncLog.grdSyncLog.SaveToFile(ExtractFilePath(ParamStr(0)) +
    '.' + ExtractFileName(ParamStr(0)) + 'log');

  if not SyncAllProcessing then
    frmSyncLog.Show;

  {
   if not SyncAllProcessing then
   begin
     if FileExists(GetUserDir + '/.unison/unison.log') then
     begin
       MessageStr := 'zCryptUI Synchronization finished at ' +
         LastTimeStamp + #13 + UnisonLogSummary + ExtraSyncMsg;
       MsgDlgEx := TMsgDlgEx.Create(MessageStr, mtInformation, [mbOK],
         Self, mrOk, 999999);
       try
         if MsgDlgEx.ShowDialog = mrOk then
           preventHide := False;
         //ShowMessage('Saving changes')
       finally
         MsgDlgEx.Free;
       end;
     end
     else  //if not FileExists(GetUserDir + '/.unison/unison.log')
     begin
       MessageStr := 'zCryptUI: Nothing to Synchronize.' + #13 + ExtraSyncMsg;
       MsgDlgEx := TMsgDlgEx.Create(MessageStr, mtInformation, [mbOK],
         Self, mrOk, 999999);
       try
         if MsgDlgEx.ShowDialog = mrOk then
           preventHide := False;
         //ShowMessage('Saving changes')
       finally
         MsgDlgEx.Free;
       end;
     end;
   end; //if not SyncAllProcessing
  }

     {
        try
        // Password parameter for zuluCrypt-cli
        ts3 := DecryptPWStr(PW[Containers[btnid].PwType]);
        SetCursorWait;

        // Show Wait Window ...
        //frmZcWait will must be created at runtime, otherwise
        //text contents will be not showing sometimes (gtk bug?)
        Application.CreateForm(TfrmZcWait, frmZcWait);
        frmZcWait.lblMsgTitle.Caption := 'Checking Password . . .';
        frmZcWait.StaticText1.Caption :=
          'Encrypted Container will be mount.' + #13#10 + 'Please wait . . .';

        Application.ProcessMessages;
        frmZcWait.ShowOnTop;

        // run zuluCrypt Command with parameters
        // https://wiki.freepascal.org/Executing_External_Programs
        // zuluCrypt-cli -o -t vcrypt -d '/mnt/DATAr7/System/CONTAINER' -m CONTAINER -e rw -p 'pwhere...'
        Application.ProcessMessages;
        if RunCommandTimeout('zuluCrypt-cli', ['-o', '-t',
          'vcrypt', '-d', syncprofile, '-m', syncprofilefmt, '-e', 'rw', '-p', ts3], outp, [],
          swoNone, 30) then
        begin
          ts3 := ''; // flush decrypted password from Memory
          SetCursorNormal;
          frmZcWait.Close;
          frmZcWait.Free;
          if frmConfig.chkFlushAfterMount.Checked = True then
            btnFlush.Click;
        end
        else
        begin
          ts3 := ''; // flush decrypted password from Memory
          btnFlushClick(Self);
          frmZcWait.Close;

          ShowMessage('Error opening container ' + syncprofilefmt + ':' + #13#10 + outp);
          SetCursorNormal;
        end;
      except
        on E: Exception do
        begin
          ts3 := ''; // flush decrypted password from Memory
          frmZcWait.Close;
          ShowMessage('Error opening container ' + syncprofilefmt + ':' +
            E.ClassName + #13#10 + E.Message);
          SetCursorNormal;
          btnFlushClick(Self);
        end;
      end;
      }

end;   //Synchronize Encrypted Containers btnSyncContainerClick


procedure TfrmMain.btnSyncAllClick(Sender: TObject);
// Synchronize ALL Encrypted Container Pairs
var
  i: integer;
  tBlink: TFPTimer;
  ExtraSyncMsgA: string; //Extra-Sync info string for log
  LastTimeStamp: string; //actual Timestamp
  dirpairexists: boolean; //semaphore for checking mounted sync-container pairs exists
  ptype: integer;
  ptypestr: string; // passwort type string

begin
  SyncNrProcessing := 0;
  SyncAllProcessing := True;
  pnlSyncInfo.Caption := 'Sync-All Processing...';
  pnlSyncInfo.Color := $0000C2FF;
  SetCursorWait;
  btnUmountAll.Enabled := False;
  btnUmountSyncDrives.Enabled := False;
  btnSyncAll.ImageIndex := 3;
  Application.ProcessMessages;

  //check if passwords for mounted sync container Pairs in ram exists,
  //if not, input password(s) for all container types before begin sync all
  for i := 1 to 9 do
  begin
    dirpairexists := DirectoryExists(Containers[i].Directory) and
      DirectoryExists(Containers[i + 10].Directory);

    ptype := Containers[i].PwType;
    ptypestr := Containers[i].PwTypeName;
    frmPrequest.lblContainerPW1.Caption := '(Type ' + ptypestr + '):';

    if PW[Containers[i].PwType] = '' then
    begin
      frmPrequest.ShowModal;
      PW[Containers[i].PwType] := EncryptPWStr(frmPrequest.edtp1.Text);
      frmPrequest.edtp1.Clear;
    end;
  end;

  if FileExists(GetUserDir + '/.unison/unison.log') then
  begin
    if CopyFile(GetUserDir + '/.unison/unison.log', GetUserDir +
      '/.unison/unison.log.old', [cffOverwriteFile]) then
      DeleteFile(GetUserDir + '/.unison/unison.log');
  end;


  //Run Extra Sync if Activated in Config
  if frmConfig.chkExtraSyncOn.Checked then
  begin
    frmConfig.btnExtraSyncClick(Self);
    ExtraSyncMsgA := 'Extra Sync EXECUTED';
    LastTimeStamp := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
  end
  else
  begin
    ExtraSyncMsgA := 'Extra Sync NOT ACTIVATED';
    LastTimeStamp := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
  end; //if extra sync checked

  //Save Extra Sync Log Info
  Containers[10].LastSyncTimeStamp := LastTimeStamp;
  Containers[10].LastSyncMessage := ExtraSyncMsgA;
  frmSyncLog.grdSyncLog.Cells[1, 10] := Containers[10].LastSyncTimeStamp;
  frmSyncLog.grdSyncLog.Cells[2, 10] := Containers[10].LastSyncMessage;



  // process batch sync for containers
  for i := 1 to 9 do
  begin
    //Mount Sync Containers for Sync-All automatically if configured
    if frmConfig.chkMntSyncAuto.Checked then
    begin
      //check and run auto mount of sync-container
      if DirectoryExists(Containers[i].Directory) and not
        DirectoryExists(Containers[i + 10].Directory) and
        FileExists(Containers[i + 10].FileName) then
      begin
        //mount Sync-Container...
        (FindComponent('btnOpenC' + IntToStr(i + 10)) as TBitBtn).Click;
        //and enable sync button if containers mounted on both sides
        if DirectoryExists(Containers[i].Directory) and
          DirectoryExists(Containers[i + 10].Directory) and
          FileExists(Containers[i].SyncProfile) then
        begin
          (FindComponent('btnSyncC' + IntToStr(i)) as TSpeedButton).Enabled := True;
          (FindComponent('btnOpenC' + IntToStr(i + 10)) as TBitBtn).ImageIndex := 1;
        end;
      end;
    end;//automount on sync-all

    //RUN SYNC
    if (FindComponent('btnSyncC' + IntToStr(i)) as TSpeedButton).Enabled = True then
    begin
      (FindComponent('btnSyncC' + IntToStr(i)) as TSpeedButton).Click;
      Application.ProcessMessages;
    end;

    //auto eject synchronized container if auto unmount checked in configuration
    if frmConfig.chkUmountSynced.Checked then
    begin
      btnContainerEjectClick((FindComponent('btnC' + IntToStr(i + 10) + 'eject') as
        TSpeedButton));
      (FindComponent('btnOpenC' + IntToStr(i + 10)) as TBitBtn).ImageIndex := 0;
    end;

  end;  //for i := 1 to 9 do


  LastSyncAllTime := FormatDateTime('yyyy-mm-dd hh:nn:ss', Now);
  btnSyncAll.Hint :=
    'Sync All Mounted Volumes and run Extra Sync if configured' +
    #13 + '(Last Overall Sync: ' + LastSyncAllTime + ')';
  btnSyncAll.ImageIndex := 8;
  SetCursorNormal;
  Application.ProcessMessages;
  syncDone := True;
  tSyncDone.Enabled := False;
  btnUmountAll.Enabled := True;
  btnUmountSyncDrives.Enabled := True;
  SyncAllProcessing := False;

  frmMain.Show;
  frmSyncLog.Show;

  {
  //this is not working because unison.log contains the sync info
  //for the last synced container
  //Solution: showing synclog form after sync all
  if FileExists(GetUserDir + '/.unison/unison.log') then
  begin
    MessageStr := 'zCryptUI Synchronization finished at ' +
      FormatDateTime('yyyy-mm-dd hh:nn:ss', Now) + #13 +
      'Click Log Button for more Details';
    MsgDlgEx := TMsgDlgEx.Create(MessageStr, mtInformation, [mbOK],
      Self, mrOk, 999999);
    try
      if MsgDlgEx.ShowDialog = mrOk then
        preventHide := False;
      //ShowMessage('Saving changes')
    finally
      MsgDlgEx.Free;
    end;
  end
  else  //if not FileExists(GetUserDir + '/.unison/unison.log')
  begin
    MessageStr := 'zCryptUI: Nothing to Synchronize.';
    MsgDlgEx := TMsgDlgEx.Create(MessageStr, mtInformation, [mbOK],
      Self, mrOk, 999999);
    try
      if MsgDlgEx.ShowDialog = mrOk then
        preventHide := False;
      //ShowMessage('Saving changes')
    finally
      MsgDlgEx.Free;
    end;
  end;
  }

end; //btnSyncAllClick

procedure TfrmMain.PopupMenu1Close(Sender: TObject);
begin
  XMLPropStorage1.Save;
end;

procedure TfrmMain.pop_alwaysOnTopClick(Sender: TObject);
begin
  if pop_alwaysOnTop.Checked then
    frmMain.FormStyle := fsSystemStayOnTop
  else
    frmMain.FormStyle := fsNormal;
  frmConfig.chkStayOnTop.Checked := pop_alwaysOnTop.Checked;
  frmMain.Show;
  PopupMenu1.Close;
end;

procedure TfrmMain.pop_ExitClick(Sender: TObject);
begin
  frmMain.Hide;
  btnFlushClick(Self);
  PopupMenu1.Close;
  Close;
end;

procedure TfrmMain.pop_ShowHideClick(Sender: TObject);
begin
  if frmMain.Visible then
    frmMain.Hide
  else
    frmMain.Show;

  PopupMenu1.Close;
end;

procedure TfrmMain.pop_startMinimizedClick(Sender: TObject);
begin
  PopupMenu1.Popup;
  frmConfig.chkStartMinimized.Checked := pop_startMinimized.Checked;
  PopupMenu1.Close;
end;

procedure TfrmMain.tBlinkTimer(Sender: TObject);
begin
  if btnSyncAll.ImageIndex = 3 then
    btnSyncAll.ImageIndex := 16
  else
    btnSyncAll.ImageIndex := 3;

  if (FindComponent('btnSyncC' + IntToStr(SyncNrProcessing)) as
    TSpeedButton).ImageIndex = 16 then
  begin
    (FindComponent('btnSyncC' + IntToStr(SyncNrProcessing)) as
      TSpeedButton).ImageIndex := 3;
    pnlSyncInfo.Caption := '        Sync in Progress';
  end
  else
  begin
    (FindComponent('btnSyncC' + IntToStr(SyncNrProcessing)) as
      TSpeedButton).ImageIndex := 16;
    pnlSyncInfo.Caption := 'Sync in Progress        ';
  end;

  Application.ProcessMessages;
end;

procedure TfrmMain.tInactivityTimer(Sender: TObject);
begin
  if frmConfig.chkAutoHide.Checked then
    if not syncdriveispresent then frmMain.Hide;
end;

procedure TfrmMain.TrayIcon1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);

var
  pos: TPoint;
begin
  pos := Mouse.CursorPos;

  case Button of
    mbRight:
    begin
      //necessary for multi monitor setup, simle popup does not work correctly!
      PopupMenu1.Popup(pos.X, pos.Y - 120);
    end;
    mbLeft:
    begin
      //pop_ShowHideClick(PopupMenu1);
      frmMain.Visible := True;
      frmMain.Show;
      frmMain.BringToFront;

    end;
  end;

  // !BUG! ...this causes Error on Exit
  // Project zcryptui raised exception class 'External: SIGSEGV' At address 420F5F
  // show Main Form anyway if always on Top checked
  //if pop_alwaysOnTop.Checked then
  //  frmMain.Show;

end;

procedure TfrmMain.zcryptui_mutex_tmpOtherInstance(Sender: TObject;
  ParamCount: integer; const Parameters: array of string);
begin
  frmMain.Show;
  //ShowMessage('zcryptui is already running. You can only start one instance of the program.');
end;

procedure TfrmMain.btnOpenContainerClick(Sender: TObject);
var
  outp: string;
  ts1, ts2, ts3: string; // temporary Strings
  ti: integer;
  ptype: integer;
  ptypestr: string; // passwort type string
  synctype: boolean;
  direxists: boolean; //semaphore for checking mounted container dir exists
begin
  // get Caller BitBtn Tag
  ti := (Sender as TBitBtn).Tag;
  if ti > 10 then
    synctype := True
  else
    synctype := False;

  direxists := DirectoryExists(Containers[ti].Directory);
  ts1 := Containers[ti].FileName;
  ts2 := ExtractFileName(ExtractFileNameWithoutExt(Containers[ti].FileName));

  if not direxists then
  begin
    btnSyncAll.ImageIndex := 8;
    frmPrequest.lblContainerPW.Caption := ts2 + ' Container Password?';

    ptype := Containers[ti].PwType;
    ptypestr := Containers[ti].PwTypeName;
    frmPrequest.lblContainerPW1.Caption := '(Type ' + ptypestr + '):';

    if PW[Containers[ti].PwType] = '' then
    begin
      frmPrequest.ShowModal;
      PW[Containers[ti].PwType] := EncryptPWStr(frmPrequest.edtp1.Text);
      frmPrequest.edtp1.Clear;
    end;
    if Length(PW[Containers[ti].PwType]) > 0 then
    begin
      try
        // Password parameter for zuluCrypt-cli
        ts3 := DecryptPWStr(PW[Containers[ti].PwType]);
        SetCursorWait;

        // Show Wait Window ...
        //frmZcWait will must be created at runtime, otherwise
        //text contents will be not showing sometimes (gtk bug?)
        Application.CreateForm(TfrmZcWait, frmZcWait);
        frmZcWait.lblMsgTitle.Caption := 'Checking Password . . .';
        frmZcWait.StaticText1.Caption :=
          'Encrypted Container will be mount.' + #13#10 + 'Please wait . . .';

        Application.ProcessMessages;
        frmZcWait.ShowOnTop;

        // run zuluCrypt Command with parameters
        // https://wiki.freepascal.org/Executing_External_Programs
        // zuluCrypt-cli -o -t vcrypt -d '/mnt/DATAr7/System/CONTAINER' -m CONTAINER -e rw -p 'pwhere...'
        Application.ProcessMessages;
        if RunCommandTimeout('zuluCrypt-cli',
          ['-o', '-t', 'vcrypt', '-d', ts1, '-m', ts2, '-e', 'rw', '-p', ts3],
          outp, [], swoNone, 30) then

        begin
          ts3 := ''; // flush decrypted password from Memory
          SetCursorNormal;
          frmZcWait.Close;
          frmZcWait.Free;
          if frmConfig.chkFlushAfterMount.Checked = True then
            btnFlush.Click;
          //append Container Mountpoint to internal Array var.
          Containers[ti].Directory := zCdrivePath(Containers[ti].FileName);
        end
        else
        begin
          ts3 := ''; // flush decrypted password from Memory
          btnFlushClick(Self);
          frmZcWait.Close;

          ShowMessage('Error opening container ' + ts2 + ':' + #13#10 + outp);
          SetCursorNormal;
          //append Container Mountpoint to internal Array var.
          Containers[ti].Directory := '';
        end;
      except
        on E: Exception do
        begin
          ts3 := ''; // flush decrypted password from Memory
          frmZcWait.Close;
          ShowMessage('Error opening container ' + ts2 + ':' +
            E.ClassName + #13#10 + E.Message);
          SetCursorNormal;
          //append Container Mountpoint to internal Array var.
          Containers[ti].Directory := '';
          btnFlushClick(Self);
        end;
      end;
    end;
  end
  else  // if dir already mounted
    OpenURL(Containers[ti].Directory);
end;

procedure TfrmMain.btnContainerEjectClick(Sender: TObject);
var
  outp: string;
  ts1, ts2: string; // temporary Strings
  id: integer; //button id 1-9,11-19; generated from component tag
  synctype: boolean;
  direxists: boolean;

begin
  (Sender as TSpeedButton).Enabled := False;

  // get Caller BitBtn Tag
  id := (Sender as TSpeedButton).Tag - 100;
  // check for synctype eject
  if id > 10 then
    synctype := True
  else
    synctype := False;

  // Check if Container mounted
  direxists := DirectoryExists(Containers[id].Directory);
  ts1 := Containers[id].FileName;
  ts2 := ExtractFileName(ExtractFileNameWithoutExt(Containers[id].FileName));

  if InRange(id, 1, 19) and (id <> 10) then
  begin
    SetCursorWait;
    // Check if Container mounted
    if direxists then
    begin
      //refresh microDisplay value with IdleTimer1Timer(Self) before unmount container
      tPeriodicIdleTimerTimer(Self);
      SetCursorWait;
      //frmZcWait will must be created at runtime, otherwise
      //text contents will be not showing sometimes (gtk bug?)
      Application.CreateForm(TfrmZcWait, frmZcWait);
      frmZcWait.lblMsgTitle.Caption := 'Unmounting Drive . . .';
      frmZcWait.StaticText1.Caption :=
        'Encrypted Container will be unmount.' + #13#10 + 'Please wait . . .';
      frmZcWait.lblMsgTitle.Cursor := crHourGlass;

      Application.ProcessMessages;
      //frmZcWait.ShowOnTop;

      Application.ProcessMessages;
      if RunCommandTimeout('zuluCrypt-cli', ['-q', '-t', 'vcrypt', '-d', ts1],
        outp, [], swoNone, 15) then
      begin
        SetCursorNormal;
        frmZcWait.Close;
        frmZcWait.Free;
        Containers[id].Directory := '';
      end
      else
      begin
        frmZcWait.Close;
        //ShowMessage('Error ejecting container ' + ts2 + ':' + #13#10 + outp);
        SetCursorNormal;
        if MessageDlg('ContainerGui:Drive is busy', 'The ' + ts2 +
          ' Drive is used by other Programs. ' + #13#10 +
          'Close all Documents and Applications that access the WOF-Drive.' +
          #13#10 + outp, mtWarning, [mbYes], 0) = mrYes then
        ;
      end;

    end;

  end;
  (Sender as TSpeedButton).Enabled := True;
end;

procedure TfrmMain.btnUmountAllClick(Sender: TObject);
var
  i: integer; //common loop index
begin
  SetCursorWait;

  //CHECK FOR MOUNTED CONTAINERS AND EJECT THEM
  for i := 1 to 9 do
  begin
    if (FindComponent('btnC' + IntToStr(i) + 'eject') as TSpeedButton).Enabled =
      True then
    begin   //each enabled eject button
      (FindComponent('btnC' + IntToStr(i) + 'eject') as TSpeedButton).Click;
    end;
  end;

  SetCursorNormal;
end;



procedure TfrmMain.btnFlushClick(Sender: TObject);
begin
  PW[1] := '';
  PW[2] := '';
  btnFlush.Enabled := False;
  PPh := GenPWPhrase; // Generate new random PassPhrase for Password Encryption
end;

procedure TfrmMain.btnShowLogClick(Sender: TObject);
var
  Process: TProcess;

begin //Show Unison Log
  frmSyncLog.ShowModal;

  //old variant, show unison log:
  {
  Process := TProcess.Create(nil);
  //$ xdg-open /home/lxmm/.unison/unison.log
  try
    //Process.Executable := 'xdg-open';
    Process.CommandLine := 'xdg-open' + ' /home/lxmm/.unison/unison.log';
    Process.Options := Process.Options + [poUsePipes, poWaitOnExit];
    Process.Execute;

      // while Process.ExitStatus < 0 do
      // begin //use this instead of process.Option poWaitOnExit
      //   //wait until last process ended
      //   Application.ProcessMessages;
      // end;

    Process.Free;
  except
    Process.Free;
    ShowMessage('Exception while opening Unison Log File');
  end; //try
  }

end;

procedure TfrmMain.btnCfgClick(Sender: TObject);
begin
  frmConfig.ShowModal;
end;

procedure TfrmMain.btnInfoClick(Sender: TObject);
begin
  //Poweredby1.ShowPoweredByForm;
  SplashAbout1.ShowAbout;
end;

procedure TfrmMain.ApplicationProperties1UserInput(Sender: TObject; Msg: cardinal);
begin
  //sync Done checked Symbol: reset to normal sync-all-symbol delayed,
  //if no user input happens (with tSyncDone)
  if syncDone = True then
  begin
    tSyncDone.Enabled := False;
    tSyncDone.Enabled := True;
  end;
end;

procedure TfrmMain.ApplicationProperties1Deactivate(Sender: TObject);
begin
  //Hide Main Form if not Focused and in Config Activated
  if frmConfig.chkHideOnFocusLost.Checked then
    frmMain.Hide;
end;

procedure TfrmMain.btnUmountSyncDrivesClick(Sender: TObject);
var
  mp: ansistring; //mount Point for Path
  rs: ansistring;  //return String at unmounting
  i: integer; //common loop index
begin
  SetCursorWait;

  //CHECK FOR MOUNTED CONTAINERS AND EJECT THEM
  for i := 1 to 9 do
  begin
    if (FindComponent('btnC' + IntToStr(i + 10) + 'eject') as
      TSpeedButton).Enabled = True then
    begin   //each enabled eject button
      (FindComponent('btnC' + IntToStr(i + 10) + 'eject') as TSpeedButton).Click;
    end;
  end;


  {
   //EXTERNAL DRIVE HANDLING (Mount if Ejected, Ejecting after umount all external sync containers)
   //if Length(frmConfig.edtExtDrivePath.Directory) > 2 then
   if btnUmountSyncDrives.ImageIndex = 0 then
   begin
     //ShowMessage('...'+frmConfig.edtExtDrivePath.Directory);
     // find mount Point from Path as Block Device
     if RunCommand('/bin/bash', ['-c', 'findmnt --noheadings --output SOURCE ' +
       frmConfig.edtExtDrivePath.Directory], mp) then
     begin

       //ShowMessage(mp);
       if OutSide = False then
       begin //do this in Native Environments (no VirtualBox)
         //unmount block device
         RunCommand('/bin/bash', ['-c', 'udisksctl unmount -b ' + mp], rs);
         //ShowMessage(rs);
         //remove loop device
         RunCommand('/bin/bash', ['-c', 'udisksctl loop-delete -b ' + mp], rs);
         //if do this, the device must disconnect and connect again to work
         //RunCommand('/bin/bash', ['-c', 'udisksctl power-off -b ' + mp], rs);
       end
       else //do this in Virtualized Environments (VirtualBox)
       begin
         //unmount block device
         RunCommand('/bin/bash', ['-c', 'pkexec umount ' + mp], rs);
       end;
     end
     else
     begin
       ShowMessage('Failure: Cannot Find Mount Point of external Sync Drive');
     end;
   end
   else
   begin
     //ShowMessage('External Sync Drive Path not Configured.');
     //frmConfig.ShowModal;
     //if OutSide = True then
     //begin //do this in virtualized Environments (VirtualBox)

     //mount block device if dismounted
     RunCommand('/bin/bash', ['-c', 'pkexec mount ' +
       frmConfig.edtExtDrivePath.Directory], rs);
   end;
  }
  SetCursorNormal;

end;

//Extend Main Form to see Sync Drives and extra functions
procedure TfrmMain.btnExtendUiClick(Sender: TObject);
begin
  if frmMain.Width = uiNormalWidth then
  begin
    frmMain.Width := uiExtWidth;
    btnExtendUi.ImageIndex := 15;
  end
  else
  begin
    frmMain.Width := uiNormalWidth;
    btnExtendUi.ImageIndex := 1;
  end;
end;

end.
